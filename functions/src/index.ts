import * as firebase from 'firebase';
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as cors from 'cors';
import { generate } from 'generate-password';
import { corsOptions, hostRegion, mailTransport } from './helpers';
import { generateId, setEmailConfirmationOptions, setEmailMemberConfirmationOptions, setEmailMemberOptions } from './generators';

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

admin.initializeApp();

export const helloWorld = functions.https.onRequest((request, response) => response.send('Hello from Firebase!'));

export const sendEmailConfirmation = functions
  .region(hostRegion)
  .https
  .onRequest((request, response) => {
    const mailList = [
      'ell15evans.nuls@googlemail.com',
      'will.moorcroft@btinternet.com',
    ];
    const mailOptions = setEmailConfirmationOptions(mailList, request.body);

    return cors(corsOptions)(request, response, () =>
      mailTransport
        .sendMail(mailOptions)
        .then(() => response.status(200).send({message: 'email has been sent'}))
        .catch((error: string) => response.status(400).send(`There was an error while sending the email: ${error}`)));

  });

export const sendMembership = functions
  .region(hostRegion)
  .https
  .onRequest((request, response) => {
    const membershipInfo = request.body;
    membershipInfo.id = generateId();
    membershipInfo.password = generate({length: 10, numbers: true});

    const mailList = [
      'ell15evans.nuls@googlemail.com',
      'fi.clements22@gmail.com'
    ];
    const mailOptions = setEmailMemberOptions(mailList, membershipInfo);
    const mailMemberOptions = setEmailMemberConfirmationOptions(mailList, membershipInfo);

    return cors(corsOptions)(request, response, () =>
      Promise.all([mailTransport.sendMail(mailOptions), mailTransport.sendMail(mailMemberOptions)]))
      .then(() => {
        response
          .status(200)
          .send({
            message: 'Your submission has been sent and is awaiting validation. You will receive an email confirming your ' +
              'registration shortly with important information'
          });
      })
      .catch((error: string) => response.status(400).send(`There was an error while sending the email: ${error}`));
  });

export const createMembersReport = functions
  .region(hostRegion)
  .https
  .onRequest((request, response) => {
    const membersInfo = request.body;
    const storageRef = firebase.storage().ref();
    const file = `reports/members_report_${new Date().toUTCString()}.json`;
    const mountainImagesRef = storageRef.child(file);

    return cors(corsOptions)(request, response, () =>
      Promise.all([mountainImagesRef.put(membersInfo), storageRef.child(file).getDownloadURL()]))
      .then((data: any) => {
        response
          .status(200)
          .send(data);
      })
      .catch((error: string) => response.status(400).send(`There was an error while storing and downloading the report: ${error}`));

  });
