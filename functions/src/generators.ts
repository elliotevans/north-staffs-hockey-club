import { emailTemplate, EmailTemplates } from './templates';
import { nshcEmail } from './helpers';

export const setEmailConfirmationOptions = (mailList, message) => ({
  from: nshcEmail,
  to: mailList,
  subject: 'Contact from the North Stafford Hockey Website',
  html: emailTemplate(EmailTemplates.CONFIRMATION, message)
});

export const setEmailMemberOptions = (mailList, membershipInfo) => ({
  from: nshcEmail,
  to: mailList,
  subject: 'Membership form from the North Stafford Hockey Website',
  html: emailTemplate(EmailTemplates.MEMBER_SENDBACK, membershipInfo)
});

export const setEmailMemberConfirmationOptions = (mailList, membershipInfo) => ({
  from: '"North Stafford Hockey Club" <contact@northstaffordhockeyclub.co.uk>',
  to: membershipInfo.email,
  subject: 'Membership form from the North Stafford Hockey Website',
  html: emailTemplate(EmailTemplates.MEMBER_CONFIRMATION, membershipInfo)
});

export const generateId = () => Math.floor(1000 + Math.random() * 9000);
