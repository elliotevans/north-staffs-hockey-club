export interface Message {
  firstName: string;
  lastName: string;
  emailAddress: string;
  subject: string;
  description: string;
  date: Date;
}
