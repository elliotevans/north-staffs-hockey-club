import * as functions from 'firebase-functions';
import * as nodemailer from 'nodemailer';

export const hostRegion = 'europe-west1';
export const gmailEmail = functions.config().gmail.email;
export const gmailPassword = functions.config().gmail.password;
export const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword,
  },
});
export const nshcEmail = '"North Stafford Hockey Club" <contact@northstaffordhockeyclub.co.uk>';
export const corsOptions = {
  origin: '*',
  allowedHeaders: ['Content-Type', 'Authorization', 'Content-Length', 'X-Requested-With', 'Accept'],
  methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
  optionsSuccessStatus: 200
};
