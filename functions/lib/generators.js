"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const templates_1 = require("./templates");
const helpers_1 = require("./helpers");
exports.setEmailConfirmationOptions = (mailList, message) => ({
    from: helpers_1.nshcEmail,
    to: mailList,
    subject: 'Contact from the North Stafford Hockey Website',
    html: templates_1.emailTemplate(templates_1.EmailTemplates.CONFIRMATION, message)
});
exports.setEmailMemberOptions = (mailList, membershipInfo) => ({
    from: helpers_1.nshcEmail,
    to: mailList,
    subject: 'Membership form from the North Stafford Hockey Website',
    html: templates_1.emailTemplate(templates_1.EmailTemplates.MEMBER_SENDBACK, membershipInfo)
});
exports.setEmailMemberConfirmationOptions = (mailList, membershipInfo) => ({
    from: '"North Stafford Hockey Club" <contact@northstaffordhockeyclub.co.uk>',
    to: membershipInfo.email,
    subject: 'Membership form from the North Stafford Hockey Website',
    html: templates_1.emailTemplate(templates_1.EmailTemplates.MEMBER_CONFIRMATION, membershipInfo)
});
exports.generateId = () => Math.floor(1000 + Math.random() * 9000);
//# sourceMappingURL=generators.js.map