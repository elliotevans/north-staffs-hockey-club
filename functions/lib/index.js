"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebase = require("firebase");
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const cors = require("cors");
const generate_password_1 = require("generate-password");
const helpers_1 = require("./helpers");
const generators_1 = require("./generators");
// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript
admin.initializeApp();
exports.helloWorld = functions.https.onRequest((request, response) => response.send('Hello from Firebase!'));
exports.sendEmailConfirmation = functions
    .region(helpers_1.hostRegion)
    .https
    .onRequest((request, response) => {
    const mailList = [
        'ell15evans.nuls@googlemail.com',
        'will.moorcroft@btinternet.com',
    ];
    const mailOptions = generators_1.setEmailConfirmationOptions(mailList, request.body);
    return cors(helpers_1.corsOptions)(request, response, () => helpers_1.mailTransport
        .sendMail(mailOptions)
        .then(() => response.status(200).send({ message: 'email has been sent' }))
        .catch((error) => response.status(400).send(`There was an error while sending the email: ${error}`)));
});
exports.sendMembership = functions
    .region(helpers_1.hostRegion)
    .https
    .onRequest((request, response) => {
    const membershipInfo = request.body;
    membershipInfo.id = generators_1.generateId();
    membershipInfo.password = generate_password_1.generate({ length: 10, numbers: true });
    const mailList = [
        'ell15evans.nuls@googlemail.com',
        'fi.clements22@gmail.com'
    ];
    const mailOptions = generators_1.setEmailMemberOptions(mailList, membershipInfo);
    const mailMemberOptions = generators_1.setEmailMemberConfirmationOptions(mailList, membershipInfo);
    return cors(helpers_1.corsOptions)(request, response, () => Promise.all([helpers_1.mailTransport.sendMail(mailOptions), helpers_1.mailTransport.sendMail(mailMemberOptions)]))
        .then(() => {
        response
            .status(200)
            .send({
            message: 'Your submission has been sent and is awaiting validation. You will receive an email confirming your ' +
                'registration shortly with important information'
        });
    })
        .catch((error) => response.status(400).send(`There was an error while sending the email: ${error}`));
});
exports.createMembersReport = functions
    .region(helpers_1.hostRegion)
    .https
    .onRequest((request, response) => {
    const membersInfo = request.body;
    const storageRef = firebase.storage().ref();
    const file = `reports/members_report_${new Date().toUTCString()}.json`;
    const mountainImagesRef = storageRef.child(file);
    return cors(helpers_1.corsOptions)(request, response, () => Promise.all([mountainImagesRef.put(membersInfo), storageRef.child(file).getDownloadURL()]))
        .then((data) => {
        response
            .status(200)
            .send(data);
    })
        .catch((error) => response.status(400).send(`There was an error while storing and downloading the report: ${error}`));
});
//# sourceMappingURL=index.js.map