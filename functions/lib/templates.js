"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EmailTemplates;
(function (EmailTemplates) {
    EmailTemplates["CONFIRMATION"] = "confirmation";
    EmailTemplates["MEMBER_SENDBACK"] = "member sendback";
    EmailTemplates["MEMBER_CONFIRMATION"] = "member confirmation";
})(EmailTemplates = exports.EmailTemplates || (exports.EmailTemplates = {}));
exports.NSHC_LOGO = 'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/nshc-logo.png?' +
    'alt=media&token=937dc026-x465e-4680-a0e2-31fbe5506b89';
exports.emailConfirmationTemplate = (message) => `
    <table style="margin: 15px 0;border: 1px solid black;border-collapse: collapse;">
        <thead style="text-align: center;font-weight: bold;">
            <td style="border: 1px solid black;border-collapse: collapse;">First Name</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Last Name</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Email</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Subject</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Description</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Date</td>
        </thead>
        <tbody>
            <td style="border: 1px solid black;border-collapse: collapse;">${message.firstName}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${message.lastName}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${message.emailAddress}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${message.subject}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${message.description}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${message.date}</td>
        </tbody>
    </table>
`;
exports.emailMemberSendbackTemplate = (membershipInfo) => `
    <h1>Somebody has submitted a membership form</h1>
    <h5>Date: TBD</h5>
    <table style="margin: 15px 0;border: 1px solid black;border-collapse: collapse;">
        <thead style="text-align: center;font-weight: bold;">
            <td style="border: 1px solid black;border-collapse: collapse;">Surname</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Given Name</td>
            <td style="border: 1px solid black;border-collapse: collapse;">First Address</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Second Address</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Town / City</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Postcode</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Email</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Twitter</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Phone Number</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Date of Birth</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Gender</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Ethnicity</td>
        </thead>
        <tbody>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.surname}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.givenName}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.firstAddress}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.secondAddress}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.townCity}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.postcode}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.email}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.twitter}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.phoneNumber}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.dateOfBirth}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.gender}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.ethnicity}</td>
        </tbody>
    </table>
    <table>
        <thead >
            <td style="border: 1px solid black;border-collapse: collapse;">Disability</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Occupation</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Gender</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Doctor</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Emergency Contact Name</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Emergency Contact Number</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Emergency Contact Relationship</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Medical History</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Umpire</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Coach</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Comments</td>
        </thead>
        <tbody>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.disability}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.occupation}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.gender}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.doctor}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.emergencyContactName}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.emergencyContactNumber}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.emergencyContactRelationship}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.medicalHistory}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.umpire}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.coach}</td>
            <td style="border: 1px solid black;border-collapse: collapse;">${membershipInfo.comments}</td>
        </tbody>
    </table>
`;
exports.emailMemberConfirmationTemplate = (membershipInfo) => `
    <h1>Thank you for sending the membership form, congratulations you have now been registered as a NSHC member!</h1>

    <p>Your Member Number: ${membershipInfo.id}</p>
    <p>Your password: ${membershipInfo.password}</p>

    <p>Your Member Number will serve as your unique identifier within NSHC so please quote it on all correspondence such as payment references
    and the like. Your Member Number will also become your username when logging into the NSHC website with the password provided.</p>

    <p>This feature has not been enabled as yet, however the site will be enhanced with a members only area in due course for which you will
    need to login with these credentials so please retain them.</p>

    <p>Below are the annual membership subscription payment details for the 2019/20 season:</p>

    <table style="margin: 15px 0;border: 1px solid black;border-collapse: collapse;">
        <thead style="text-align: center;font-weight: bold;">
            <td style="border: 1px solid black;border-collapse: collapse;">Membership Level</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Payment Option</td>
            <td style="border: 1px solid black;border-collapse: collapse;">If paid on/before 31 Oct 2019</td>
            <td style="border: 1px solid black;border-collapse: collapse;">If paid after 31 Oct 2019</td>
        </thead>
        <tbody>
            <tr>
                <td style="border: 1px solid black;border-collapse: collapse;">Senior</td>
                <td style="border: 1px solid black;border-collapse: collapse;">Option 1: £250 up-front payment for regular season
                matches* and training</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£250</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£280</td>
            </tr>
            <tr>
                <td style="border: 1px solid black;border-collapse: collapse;"></td>
                <td style="border: 1px solid black;border-collapse: collapse;">Option 2: £270 by way of 6 payments of £45 by monthly standing
                order for regular season matches* and training.</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£45/month</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£50/month</td>
            </tr>
            <tr>
                <td style="border: 1px solid black;border-collapse: collapse;"></td>
                <td style="border: 1px solid black;border-collapse: collapse;">Option 3: £130 up-front and pay-as-you-play at £10 per game</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£130</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£160</td>
            </tr>
            <tr></tr>
            <tr>
                <td style="border: 1px solid black;border-collapse: collapse;">Junior (U18) / Unemployed</td>
                <td style="border: 1px solid black;border-collapse: collapse;">Option 1: £140 up-front payment regular season matches*
                and training</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£150</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£165</td>
            </tr>
            <tr>
                <td style="border: 1px solid black;border-collapse: collapse;"></td>
                <td style="border: 1px solid black;border-collapse: collapse;">Option 2: £150 by way of 6 payments of £25 by monthly standing
                order for regular season matches* and training.</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£27.50/month</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£30/month</td>
            </tr>
            <tr>
                <td style="border: 1px solid black;border-collapse: collapse;"></td>
                <td style="border: 1px solid black;border-collapse: collapse;">Option 3: £70 up-front and pay-as-you-play at £6 per game</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£85</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£100</td>
            </tr>
            <tr></tr>
            <tr>
                <td style="border: 1px solid black;border-collapse: collapse;">Student</td>
                <td style="border: 1px solid black;border-collapse: collapse;">Option 1: £110.00 up-front for regular season matches* and
                training</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£110</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£125</td>
            </tr>
            <tr>
                <td style="border: 1px solid black;border-collapse: collapse;"></td>
                <td style="border: 1px solid black;border-collapse: collapse;">Option 2: £120 by way of 6 payments of £20 by monthly standing
                order for regular season matches* and training.</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£20/month</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£22.50/month</td>
            </tr>
            <tr>
                <td style="border: 1px solid black;border-collapse: collapse;"></td>
                <td style="border: 1px solid black;border-collapse: collapse;">Option 3: £35.00 up front and then pay-as-you-play at £6.00
                per game</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£35</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£50</td>
            </tr>
            <tr></tr>
            <tr>
                <td style="border: 1px solid black;border-collapse: collapse;">Social</td>
                <td style="border: 1px solid black;border-collapse: collapse;">Non-player’s access to Clubhouse and discounted rates to Club
                events</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£25</td>
                <td style="border: 1px solid black;border-collapse: collapse;text-align: right;">£25</td>
            </tr>
        </tbody>
    </table>
    
    <p>* Regular season matches includes league matches and pre-season friendlies but excludes any additional matches such as cup competitions or summer league games which 
    will be charge for separately on a game by game basis.</p>
    
    <p>** To support any goalkeepers whom pay for and provide their own goalkeeping kit, we are offering a 50% discount to the overall subscription cost in the case of 
    options 1 and 2, and a 50% discount to the up-front cost only in the case of option 3 (match fees remain at the non-discounted level), further encouraging members to select 
    a payment option that doesn’t require the collection of weekly match fees.</p>
    
    <div>
      <h5>Bank Transfer details are as follows:</h5>
  
      <div>
        <span>Sort Code:</span>
        <span>54-10-27</span>
      </div>
  
      <div>
        <span>Account Number:</span>
        <span>13795791</span>
      </div>
    </div>
    
    <strong>Please quote your Member Number in the payment reference so we know whom you are paying for.</strong>
    
    <p>Payments by cheque are accepted. Please make all cheques payable to 'North Stafford Hockey Club'. Members can pass their cheques to Craig McIntyre directly, any member 
    of the committee, or their team captain.</p>

    <p>Members wishing to pay using Option 2 by standing order will need to contact their bank either online, by phone or in branch to set up the standing order, giving the 
    bank the account number and sort code below together with the respective monthly amount to pay. Please set up the standing order for the first payment to reach the club 
    by 31st October 2019 and quote your member number in the payment reference. Remember to cancel the standing order once all 6 instalments have been made!</p>

    <p>Any membership subscription enquiries please email the following address with the word 'Subscription' in the subject line:
    contact@northstaffordhc.co.uk</p>
`;
exports.renderEmailTemplate = (template, message) => {
    switch (template) {
        case EmailTemplates.CONFIRMATION:
            return exports.emailConfirmationTemplate(message);
        case EmailTemplates.MEMBER_CONFIRMATION:
            return exports.emailMemberConfirmationTemplate(message);
        case EmailTemplates.MEMBER_SENDBACK:
            return exports.emailMemberSendbackTemplate(message);
        default:
            return '';
    }
};
exports.emailTemplate = (template, message) => `
    <body>
        <table>
            <thead style="background: #000531">
                <td style="width: 60%">
                    <h1 style="color: white">North Stafford Hockey Club</h1>

                </td>
                <td style="width: 40%">
                    <img src="${exports.NSHC_LOGO}" alt="North Stafford Hockey Club Logo">
                </td>
            </thead>
            <tbody style="padding: 80px; border: 1px solid black;">
                ${exports.renderEmailTemplate(template, message)}
            </tbody>
            <tfoot>
                <p style="text-align: center">
                    © 2019 North Stafford Hockey Club. All Rights Reserved. ‘North Stafford Hockey Club’ and ‘NSHC’ are trading styles of
                    North Stafford Hockey Club Limited. A Company registered in England & Wales. (Co. Regn. No. 06176824). Registered Office:
                    12 Eleanor View, Newcastle-under-Lyme, ST5 3SD.
                </p>
            </tfoot>
        </table>
    </body>
`;
//# sourceMappingURL=templates.js.map