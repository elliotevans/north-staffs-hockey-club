"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const nodemailer = require("nodemailer");
exports.hostRegion = 'europe-west1';
exports.gmailEmail = functions.config().gmail.email;
exports.gmailPassword = functions.config().gmail.password;
exports.mailTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: exports.gmailEmail,
        pass: exports.gmailPassword,
    },
});
exports.nshcEmail = '"North Stafford Hockey Club" <contact@northstaffordhockeyclub.co.uk>';
exports.corsOptions = {
    origin: '*',
    allowedHeaders: ['Content-Type', 'Authorization', 'Content-Length', 'X-Requested-With', 'Accept'],
    methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
    optionsSuccessStatus: 200
};
//# sourceMappingURL=helpers.js.map