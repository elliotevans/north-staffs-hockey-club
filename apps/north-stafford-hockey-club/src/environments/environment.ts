import { version } from '../../../../package.json';

export const environment = {
  production: false,
  gaTrackingCode: 'UA-124171351-1',
  googleAnalytics: false,
  firebase: {
    apiKey: 'AIzaSyC9yPhOTeT9iKyxSYYm82JgUn0mwXmkH7U',
    authDomain: 'north-staffs-hockey-club.firebaseapp.com',
    databaseURL: 'https://north-staffs-hockey-club.firebaseio.com',
    projectId: 'north-staffs-hockey-club',
    storageBucket: 'north-staffs-hockey-club.appspot.com',
    messagingSenderId: '390611663192'
  },
  version: version
};
