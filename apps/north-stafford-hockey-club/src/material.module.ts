import { NgModule } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
  imports: [
    MatTooltipModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatDialogModule,
    MatTabsModule,
    MatCheckboxModule,
    MatRadioModule
  ],
  exports: [
    MatTooltipModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatDialogModule,
    MatTabsModule,
    MatCheckboxModule,
    MatRadioModule
  ],
})
export class MaterialModule { }
