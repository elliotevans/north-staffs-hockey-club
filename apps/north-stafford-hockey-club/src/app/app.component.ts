import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SeoService } from './services/seo.service';

@Component({
  selector: 'nshc-site-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  private readonly _router: Router;
  private readonly _seoService: SeoService;

  constructor(router: Router, seoService: SeoService) {
    this._router = router;
    this._seoService = seoService;
  }

  ngOnInit(): void {
    this._router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(() => {
        const snapshot: ActivatedRouteSnapshot = this._router.routerState.snapshot.root.firstChild;
        const title: string = snapshot.data['title'];
        const description: string = snapshot.data['description'];

        this._seoService.setTitle(title);
        this._seoService.updateTag('description', description, 'description');
      });
  }
}
