import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '../../../services/auth.service';
import { Member } from '../../../pages/auth/pages/profile/components/profile.component';
import { MembershipService } from '../../../pages/membership/services/membership.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'nshc-site-edit-member-dialog',
  templateUrl: 'edit-member-dialog.component.html',
  styleUrls: ['./edit-member-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditMemberDialogComponent implements OnInit, OnDestroy {
  errorMessage: string;
  editMemberForm: FormGroup;
  submitting = false;
  readonly member: Member;

  private readonly _dialogRef: MatDialogRef<EditMemberDialogComponent>;
  private readonly _formBuilder: FormBuilder;
  private readonly _authService: AuthService;
  private readonly _memberSubscription: Subscription = new Subscription();
  private readonly _afAuth: AngularFireAuth;
  private readonly _cDR: ChangeDetectorRef;
  private readonly _membershipService: MembershipService;
  private readonly _matSnackBar: MatSnackBar;

  constructor(@Inject(MAT_DIALOG_DATA) public readonly data: {member: Member},
              afAuth: AngularFireAuth,
              dialogRef: MatDialogRef<EditMemberDialogComponent>,
              formBuilder: FormBuilder,
              authService: AuthService,
              cDR: ChangeDetectorRef,
              membershipService: MembershipService,
              matSnackBar: MatSnackBar) {
    this.member = this.data.member;
    this._afAuth = afAuth;
    this._dialogRef = dialogRef;
    this._formBuilder = formBuilder;
    this._authService = authService;
    this._cDR = cDR;
    this._membershipService = membershipService;
    this._matSnackBar = matSnackBar;
  }

  ngOnInit(): void {
    this._cDR.detach();

    this.editMemberForm = new FormGroup({});

    Object
      .keys(this.member)
      .map((key) => {
        if(key === 'dateOfBirth') {
          const splitDateString = this.member[key].split("/");
          if (splitDateString[2] && splitDateString[1] ) {
            this.member[key] = `${splitDateString[2]}/${splitDateString[1]}/${splitDateString[0]}`;
          }
          return this.editMemberForm.addControl(key, new FormControl(new Date(this.member[key]).toISOString().substring(0, 10)));
        }
        return this.editMemberForm.addControl(key, new FormControl(this.member[key]));
      });

    this._cDR.detectChanges();
  }

  updateMember(): void {
    this.submitting = true;

    this._memberSubscription.add(
      this._membershipService
        .getMembership()
        .subscribe((res) => {
          const user = res.docs.filter( (doc) => doc.data()['email'] === this.member.email);
          const memberId = user[0].id;

          this._cDR.detectChanges();
          // Fixing issues with the JS Date format
          this.editMemberForm.value.dateOfBirth = new Date(this.editMemberForm.value.dateOfBirth).toLocaleDateString('en-GB');
          this._cDR.detectChanges();

          this._membershipService
            .updateDoc(memberId, this.editMemberForm.value)
            .subscribe( () => {
              this._matSnackBar.open('Details have been changed', null, { duration: 3000});
              this._cDR.detectChanges();
            });
        })
    );

  }

  get formControls() {
    return this.editMemberForm.controls;
  }

  ngOnDestroy(): void {
    this._memberSubscription.unsubscribe();
  }

}
