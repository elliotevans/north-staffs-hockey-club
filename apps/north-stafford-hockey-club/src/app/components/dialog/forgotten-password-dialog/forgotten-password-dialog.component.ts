import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'nshc-site-edit-member-dialog',
  templateUrl: 'forgotten-password-dialog.component.html',
  styleUrls: ['./forgotten-password-dialog.component.scss']
})
export class ForgottenPasswordDialogComponent implements OnInit, OnDestroy {
  errorMessage: string;
  forgottenPasswordForm: FormGroup;
  submitting = false;

  private readonly _afAuth: AngularFireAuth;
  private readonly _dialogRef: MatDialogRef<ForgottenPasswordDialogComponent>;
  private readonly _formBuilder: FormBuilder;
  private readonly _authService: AuthService;
  private readonly _passwordSubscription: Subscription = new Subscription();

  constructor(afAuth: AngularFireAuth, dialogRef: MatDialogRef<ForgottenPasswordDialogComponent>, formBuilder: FormBuilder, authService: AuthService) {
    this._afAuth = afAuth;
    this._dialogRef = dialogRef;
    this._formBuilder = formBuilder;
    this._authService = authService;
  }

  ngOnInit(): void {
    this.forgottenPasswordForm = this._formBuilder.group({
      email: [
        '',
        [
          Validators.required,
          Validators.email
        ]
      ],
    });
  }

  sendForgottenPassword(): void {
    this.submitting = true;
    const email = this.forgottenPasswordForm.value.email;

    this._passwordSubscription.add(
      this._authService.sendPasswordResetEmail(email)
      .subscribe(
        () => {
          this.submitting = false;
          this._dialogRef.close();
          // TODO Change to snackbar
          alert('Email has been sent');
        },
        (error) => {
          this.submitting = false;
          this.errorMessage = error.toString();
        }
      )
    );
  }

  ngOnDestroy(): void {
    this._passwordSubscription.unsubscribe();
  }

}
