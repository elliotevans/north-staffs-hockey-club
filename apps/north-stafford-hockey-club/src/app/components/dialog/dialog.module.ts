import { ForgottenPasswordDialogComponent } from './forgotten-password-dialog/forgotten-password-dialog.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../material.module';
import { AuthService } from '../../services/auth.service';
import { EditMemberDialogComponent } from './edit-member-dialog/edit-member-dialog.component';
import { MembershipService } from '../../pages/membership/services/membership.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    ForgottenPasswordDialogComponent,
    EditMemberDialogComponent
  ],
  entryComponents: [
    ForgottenPasswordDialogComponent,
    EditMemberDialogComponent
  ],
  providers: [
    AuthService,
    MembershipService
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class DialogModule {}
