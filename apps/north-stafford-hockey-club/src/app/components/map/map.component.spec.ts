import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MapComponent } from './map.component';
import { AppModule } from '../../app.module';
import { ContactModule } from '../../pages/contact/contact.module';
import { appRoutes } from '../../app-routing.module';

let fixture: ComponentFixture<MapComponent>;
let component: MapComponent;

beforeEach(async () => {
  TestBed.configureTestingModule({
    imports: [
      ContactModule,
      AppModule,
      RouterModule,
      RouterTestingModule.withRoutes(appRoutes)
    ],
    declarations: [],
    providers: []
  });

  fixture = TestBed.createComponent(MapComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', async () => {
  expect(component).toBeTruthy();
});

afterEach(async () => {
  fixture.destroy();
});
