import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
  selector: 'nshc-site-map',
  templateUrl: './map.component.html',
  styleUrls: ['map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapComponent implements OnInit {
  private readonly _cDR: ChangeDetectorRef;

  constructor(cDR: ChangeDetectorRef) {
    this._cDR = cDR;
  }

  ngOnInit() {
    this._cDR.detach();
  }

}
