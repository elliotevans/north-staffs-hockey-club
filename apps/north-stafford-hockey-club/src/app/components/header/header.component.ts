import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'nshc-site-header',
  templateUrl: './header.component.html',
  styleUrls: ['header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
  private readonly _route: ActivatedRoute;
  private readonly _router: Router;
  private readonly _cDR: ChangeDetectorRef;

  constructor(route: ActivatedRoute, router: Router, cDR: ChangeDetectorRef) {
    this._route = route;
    this._router = router;
    this._cDR = cDR;
  }

}
