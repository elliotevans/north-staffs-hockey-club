import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'nshc-site-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent implements OnInit {
  private readonly _cDR: ChangeDetectorRef;

  readonly version: string = environment.version;

  constructor(cDR: ChangeDetectorRef) {
    this._cDR = cDR;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this._cDR.detectChanges();
  }
}
