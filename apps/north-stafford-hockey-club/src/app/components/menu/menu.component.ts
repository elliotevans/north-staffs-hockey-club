import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'nshc-site-menu',
  templateUrl: 'menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, AfterViewInit, OnDestroy {
  loggedIn: boolean;
  isUserAdmin: boolean;
  sideNav = false;

  private readonly _cDR: ChangeDetectorRef;
  private readonly _afAuth: AngularFireAuth;
  private readonly _userSubscription: Subscription = new Subscription();
  private readonly _routerSubscription: Subscription = new Subscription();
  private readonly _router: Router;
  private readonly _authService: AuthService;

  constructor(cDR: ChangeDetectorRef, afAuth: AngularFireAuth, router: Router, authService: AuthService) {
    this._cDR = cDR;
    this._afAuth = afAuth;
    this._router = router;
    this._authService = authService;
  }

  ngOnInit(): void {
    // TODO Change to a behaviour subject to get if the user is authed. This is set by the current isAuth method
    // but will also change the value of the Behaviour subject
    this._userSubscription.add(
      this._authService
        .getUser()
        .subscribe((res) => {
          if (res) {
            this.loggedIn = true;
            this.isUserAdmin = res.email === "ell15evans.nuls@googlemail.com" || res.email === "jim.Venn@version1.com";
            this._cDR.detectChanges();
          } else {
            this.loggedIn = false;
            this._cDR.detectChanges();
          }
        })
    );

    this._routerSubscription.add(
      this._router.events.subscribe( (res) => {
        this.sideNav = false;
        this._cDR.detectChanges();
      })
    );
  }

  ngAfterViewInit(): void {
    this._cDR.detach();
  }

  logoutUser() {
    this._userSubscription.add(
      this._authService
        .signOut()
        .subscribe( () => {
          this._cDR.detectChanges();
          this._router.navigate(['/']);
        })
    );
  }

  toggleSideNav(): void {
    this.sideNav = !this.sideNav;
    this._cDR.detectChanges();
  }

  ngOnDestroy(): void {
    this._userSubscription.unsubscribe();
    this._routerSubscription.unsubscribe();
  }

}
