import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuComponent } from './menu.component';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from '../../app.module';
import { appRoutes } from '../../app-routing.module';

let fixture: ComponentFixture<MenuComponent>;
let component: MenuComponent;

beforeEach(async () => {
  TestBed.configureTestingModule({
    imports: [
      AppModule,
      RouterModule,
      RouterTestingModule.withRoutes(appRoutes)
    ],
    declarations: [],
    providers: []
  });

  fixture = TestBed.createComponent(MenuComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', async () => {
  expect(component).toBeTruthy();
});

afterEach(async () => {
  fixture.destroy();
});
