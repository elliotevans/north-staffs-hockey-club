import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { CardComponent } from './card.component';
import { By } from '@angular/platform-browser';
import { AppModule } from '../../app.module';
import { appRoutes } from '../../app-routing.module';

let fixture: ComponentFixture<CardComponent>;
let component: CardComponent;

beforeEach(async () => {
  TestBed.configureTestingModule({
    imports: [
      AppModule,
      RouterModule,
      RouterTestingModule.withRoutes(appRoutes)
    ],
    declarations: [],
    providers: []
  }).compileComponents();

  fixture = TestBed.createComponent(CardComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', async () => {
  expect(component).toBeTruthy();
});

test('the component correctly displays the cardDetails', async () => {
  fixture.componentInstance.cardDetails = {
    id: 1,
    text: 'image text',
    imageSrc: 'image_source.jpg',
    href: 'link',
    imageAlt: 'alt tag'
  };

  component.ngOnInit();
  fixture.detectChanges();

  const figCaptionText = fixture.debugElement.query(By.css('figcaption')).nativeElement.textContent;
  const imageSrc = fixture.debugElement.query(By.css('img')).nativeElement.src;

  expect(imageSrc)
    .toContain(fixture.componentInstance.cardDetails.imageSrc);

  expect(fixture.componentInstance.cardDetails.text)
    .toBe(figCaptionText);
});

afterEach(async () => {
  fixture.destroy();
});
