import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { Router } from '@angular/router';
import { ICard } from '../../types/image.type';

@Component({
  selector: 'nshc-site-card',
  templateUrl: 'card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardComponent implements OnInit, OnChanges {
  @Input() cardDetails: ICard;

  test: ICard;

  private readonly _router: Router;
  private readonly _cDR: ChangeDetectorRef;

  constructor(router: Router, cDR: ChangeDetectorRef) {
    this._router = router;
    this._cDR = cDR;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this._cDR.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.test = this.cardDetails;
    // this.test = changes["cardDetails"].currentValue as ICard;
    this._cDR.markForCheck();
  }

}
