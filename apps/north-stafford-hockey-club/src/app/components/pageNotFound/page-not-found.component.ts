import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

@Component({
  selector: 'nshc-site-page-no-found',
  providers: [
    Location,
    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    }
  ],
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageNotFoundComponent implements OnInit {
  private readonly _cDR: ChangeDetectorRef;
  private readonly _location: Location;

  constructor(cDR: ChangeDetectorRef, location: Location) {
    this._cDR = cDR;
    this._location = location;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this._cDR.detectChanges();
  }

  goPrevious(): void {
    this._location.back();
  }

}
