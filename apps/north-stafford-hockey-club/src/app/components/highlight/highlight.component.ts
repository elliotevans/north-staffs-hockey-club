import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { IEvent } from '../../types/events.type';
import { EventsService } from '../../services/events.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'nshc-site-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HighlightComponent implements OnInit, OnDestroy {
  latestEvent: IEvent;

  private readonly _cDR: ChangeDetectorRef;
  private readonly _eventSubscriptions: Subscription = new Subscription();
  private readonly _eventsService: EventsService;
  private readonly _route: ActivatedRoute;

  constructor(cDR: ChangeDetectorRef, eventsService: EventsService, route: ActivatedRoute) {
    this._cDR = cDR;
    this._eventsService = eventsService;
    this._route = route;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this._cDR.detectChanges();

    this._eventSubscriptions.add(
      this._eventsService
        .valueChanges()
        .subscribe((res) => {
          const filteredEvents = res.filter((event) => event.active === true);
          const sortedEvents = filteredEvents.sort((a, b) => a.date.seconds - b.date.seconds);
          this.latestEvent = sortedEvents ? sortedEvents[0] : null;

          this._cDR.detectChanges();
        })
    );
  }

  ngOnDestroy(): void {
    this._eventSubscriptions.unsubscribe();
  }

}
