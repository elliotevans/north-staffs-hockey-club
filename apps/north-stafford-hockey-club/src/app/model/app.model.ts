class Captain {
  name: string;
  email: string;
  phone: string;
}

class Player {
  name: string;
  nickname: string;
  number: number;
  position: string;
}

export class Team {
  id: number;
  name: string;
  captain: Captain;
  players: Array<Player>;
}

export class TeamSection {
  id: number;
  name: string;
  teams: Array<Team>;
}
