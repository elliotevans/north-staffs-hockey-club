import { Actions, ActionTypes } from '../actions/app.actions';
import { Team, TeamSection } from '../model/app.model';

export interface ITeamStore {
  teams: Array<TeamSection>;
  activeCategory: TeamSection;
  activeTeam: Team;
}

export enum TeamStore {
  TEAMS = 'teams',
  ACTIVE_CATEGORY = 'activeCategory',
  ACTIVE_TEAM = 'activeTeam'
}

const teams = [
  {
    id: 1,
    name: 'mens',
    teams: [
      {
        id: 1,
        name: '1st XI',
        captain: {
          name: 'Stu Stanway',
          email: '',
          phone: '07872341406',
        },
        players: []
      },
      {
        id: 2,
        name: '2nd XI',
        captain: {
          name: 'Stu Morton',
          email: '',
          phone: '07595914911',
        },
        players: []
      },
      {
        id: 3,
        name: '3rd XI',
        captain: {
          name: 'Ben Furnival',
          email: '',
          phone: '07551123045',
        },
        players: []
      },
      {
        id: 4,
        name: '4th XI',
        captain: {
          name: 'Stu Flecher',
          email: '',
          phone: '07979960109',
        },
        players: []
      },
      {
        id: 5,
        name: '5th XI',
        captain: {
          name: 'Adam Dean',
          email: '',
          phone: '07745663125',
        },
        players: []
      }
    ]
  },
  {
    id: 2,
    name: 'ladies',
    teams: [
      {
        id: 1,
        name: '1st XI',
        captain: {
          name: 'Emily Dulson',
          email: '',
          phone: '07841411048',
        },
        players: []
      },
      {
        id: 2,
        name: '2nd XI',
        captain: {
          name: 'Helen Heyburn',
          email: '',
          phone: '07841411048',
        },
        players: []
      }
    ]
  },
  {
    id: 3,
    name: 'junior',
    teams: [
      {
        id: 1,
        name: 'Badgers',
        captain: {
          name: 'Lee Royal',
          email: '',
          phone: '07533722145',
        },
        players: []
      },
    ]
  },
  {
    id: 4,
    name: 'indoor',
    teams: []
  },
  {
    id: 5,
    name: 'summer league',
    teams: []
  }
] as Array<TeamSection>;

const activeCategory = null as TeamSection;

const activeTeam = null as Team;

export const initialState: ITeamStore = {
  teams,
  activeCategory,
  activeTeam
};

export function reducer(state = initialState, action: Actions) {
  switch (action.type) {
    case ActionTypes.SELECT_SECTION:
      return {...state, activeCategory: action.payload};
    case ActionTypes.SELECT_TEAM:
      return {...state, activeTeam: action.payload};
    default:
      return state;
  }
}
