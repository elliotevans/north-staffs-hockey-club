import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { auth, firestore, User } from 'firebase';
import { fromPromise } from 'rxjs/internal-compatibility';
import { map, mergeMap } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';
import { FirestoreCollectionPaths, IdEmailCollection } from '../types/firestore-collection.types';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { IEvent } from '../types/events.type';

@Injectable()
export class AuthService {
  private readonly _afAuth: AngularFireAuth;
  private readonly _afDatabase: AngularFireDatabase;
  private readonly _idEmailCollection: AngularFirestoreCollection<IEvent>;

  constructor(afAuth: AngularFireAuth, aFDatabase: AngularFireDatabase, aFirestore: AngularFirestore) {
    this._afAuth = afAuth;
    this._afDatabase = aFDatabase;
    this._idEmailCollection = aFirestore.collection(FirestoreCollectionPaths.ID_EMAIL);
  }

  getUser(): Observable<User> {
    return this._afAuth.user;
  }

  isCurrentUser(): boolean {
    return !!this._afAuth.auth.currentUser;
  }

  isAuthenticated(): Observable<boolean> {
    return this._afAuth.authState
      .pipe(map( (res) => !!res));
  }

  createUserWithEmailAndPassword(email: string, password: string): Observable<auth.UserCredential> {
    const createUser = this._afAuth.auth.createUserWithEmailAndPassword(email, password);

    return fromPromise(createUser);
  }

  signInWithEmailAndPassword(email: string, password: string): Observable<auth.UserCredential> {
    const signIn = this._afAuth.auth.signInWithEmailAndPassword(email, password);

    return fromPromise(signIn);
  }

  signInWithIdAndPassword(id, password: string): Observable<auth.UserCredential> {
    return this._idEmailCollection
      .get()
      .pipe(
            map((users: firestore.QuerySnapshot) => {
              const matchedUser = users.docs.filter((user) => user.data().id.toString() === id);

              if(matchedUser.length > 0) {
                return matchedUser[0].data();
              } else {
                throw new Error('no user with id: ' + id + ' found');
              }
            }),
            mergeMap( (user: IdEmailCollection) => this.signInWithEmailAndPassword(user.email, password))
      )
  }

  signOut(): Observable<void> {
    const signOut = this._afAuth.auth.signOut();

    return fromPromise(signOut);
  }

  verifyMail(oobCode: string): Observable<void> {
    return fromPromise(this._afAuth.auth.checkActionCode(oobCode))
      .pipe(mergeMap( () => fromPromise(this._afAuth.auth.applyActionCode(oobCode))));
  }

  sendPasswordResetEmail(email: string, options = null): Observable<void> {
    const sendPasswordReset = this._afAuth.auth.sendPasswordResetEmail(email, options);

    return fromPromise(sendPasswordReset);
  }

  changePassword(code: string, password: string): Observable<void> {
    const confirmPassword = this._afAuth.auth.confirmPasswordReset(code, password);

    return fromPromise(confirmPassword);
  }

  isAdmin(): Observable<boolean> {
    return this._afAuth.authState
      .pipe(map( (user) => user && user.email === 'ell15evans.nuls@googlemail.com' || user && user.email === 'Jim.Venn@version1.com'));
  }

}
