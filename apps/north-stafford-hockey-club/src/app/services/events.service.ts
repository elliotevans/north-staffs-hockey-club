import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { FirestoreCollectionPaths } from '../types/firestore-collection.types';
import { Observable, of } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { IEvent } from '../types/events.type';
import { firestore } from 'firebase';

@Injectable()
export class EventsService {
  private readonly _afAuth: AngularFireAuth;
  private readonly _events: AngularFirestoreCollection<IEvent>;

  constructor(afAuth: AngularFireAuth, aFirestore: AngularFirestore) {
    this._afAuth = afAuth;
    this._events = aFirestore.collection(FirestoreCollectionPaths.EVENTS);
  }

  get(): Observable<firestore.QuerySnapshot> {
    return this._events
      .get();
  }

  add(data: IEvent): Observable<DocumentReference> {
    return fromPromise(
      this._events
      .add(data)
    );
  }

  updateDoc(id: string, data: IEvent): Observable<void> {
    return fromPromise(
      this._events
      .doc(id)
      .update(data)
    );
  }

  deleteDoc(id: string): Observable<void> {
    return fromPromise(
      this._events
      .doc(id)
      .delete()
    );
  }

  valueChanges(): Observable<Array<IEvent>> {
    return this._events.valueChanges() || of([]);
  }

}
