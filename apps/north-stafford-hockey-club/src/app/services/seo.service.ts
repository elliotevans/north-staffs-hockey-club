import { Meta, Title } from '@angular/platform-browser';
import { Injectable } from '@angular/core';

@Injectable()
export class SeoService {
  private readonly _titleService: Title;
  private readonly _metaService: Meta;

  constructor(titleService: Title, metaService: Meta) {
    this._titleService = titleService;
    this._metaService = metaService;
  }

  getTitle(): string {
    return this._titleService.getTitle();
  }

  setTitle(newTitle: string): void {
    this._titleService.setTitle(newTitle);
  }

  getTag(selector: string): string {
    const metaElement: HTMLMetaElement = this._metaService.getTag(`name=${selector}`);

    return metaElement.content;
  }

  updateTag(selector: string, name: string, content: string) {
    this._metaService.updateTag({name, content }, `name=${selector}`);
  }

}
