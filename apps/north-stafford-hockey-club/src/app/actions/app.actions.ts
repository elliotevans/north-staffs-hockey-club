import { Action } from '@ngrx/store';
import { Team, TeamSection } from '../model/app.model';

export enum ActionTypes {
  SELECT_SECTION = '[TEAMS] SELECT_SECTION',
  SELECT_TEAM = '[TEAMS] SELECT_TEAM',
}

export class SelectSection implements Action {
  readonly type = ActionTypes.SELECT_SECTION;

  constructor(readonly payload: TeamSection) {}
}

export class SelectTeam implements Action {
  readonly type = ActionTypes.SELECT_TEAM;

  constructor(readonly payload: Team) {}
}

export type Actions = SelectSection | SelectTeam;
