export interface ICard {
    id: number;
    text: string;
    imageSrc: string;
    imageAlt: string;
    href: string;
}
