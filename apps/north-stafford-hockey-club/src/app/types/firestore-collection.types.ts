export enum FirestoreCollectionPaths {
  EVENTS = 'events',
  ID_EMAIL = 'idEmail',
  MEMBERSHIP = 'membership'
}

export interface IdEmailCollection {
  id: number;
  email: string
}
