export interface IContactForm {
  firstName:  string;
  lastName:  string;
  emailAddress:  string;
  subject:  string;
  description:  string;
  date?: string;
}

export interface IRoute {
  name: string;
  url: string;
  componentState: number;
}
