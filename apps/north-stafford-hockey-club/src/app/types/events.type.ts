export interface IEvent {
  id: number;
  title: string;
  date: Date | any; // Firebase Date can use toDate()
  description: string;
  information: string;
  active: boolean;
  href: string; // TODO: NEW
}
