import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/components/home.component';
import { PageNotFoundComponent } from './components/pageNotFound/page-not-found.component';
import { VerifyGuard } from './pages/verify/guards/verify.guard';

export const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      title: 'Home',
      description: 'Homepage - quick overview.',
    },
  },

  {
    path: 'about',
    loadChildren: () => import('./pages/about/about.module').then(mod => mod.AboutModule),
    data: {
      title: 'About',
      description: 'About - quick overview.',
    }
  },

  {
    path: 'teams',
    loadChildren: () => import('./pages/teams/teams.module').then(mod => mod.TeamsModule),
    data: {
      title: 'Teams',
      description: 'Teams - quick overview.',
    }
  },

  {
    path: 'kit',
    loadChildren: () => import('./pages/kit/kit.module').then(mod => mod.KitModule),
    data: {
      title: 'Kit',
      description: 'Kit - quick overview.',
    }
  },

  {
    path: 'membership',
    loadChildren: () => import('./pages/membership/membership.module').then(mod => mod.MembershipModule),
    data: {
      title: 'Membership Form',
      description: 'Membership Form - quick overview.',
    }
  },

  {
    path: 'event/:eventName',
    loadChildren: () => import('./pages/event/event.module').then(mod => mod.EventModule),
    data: {
      title: 'Event',
      description: 'Event',
    }
  },

  // TODO: Have years or team split up the gallery
  {
    // path: 'gallery/:date',
    path: 'gallery',
    loadChildren: () => import('./pages/gallery/gallery.module').then(mod => mod.GalleryModule),
    data: {
      title: 'Gallery',
      description: 'Gallery - quick overview',
    }
  },

  {
    path: 'contact',
    loadChildren: () => import('./pages/contact/contact.module').then(mod => mod.ContactModule),
    data: {
      title: 'Contact',
      description: 'Contact - quick overview.',
    }
  },

  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(mod => mod.LoginModule),
    data: {
      title: 'Login',
      description: 'Login - Member Area',
    }
  },

  {
    path: 'verify',
    canActivate: [VerifyGuard],
    loadChildren: () => import('./pages/verify/verify.module').then(mod => mod.VerifyModule),
    data: {
      title: 'Verify',
      description: 'Verify - verify your email',
    }
  },

  {
    path: 'auth',
    // canLoad: [AuthGuard],
    loadChildren: () => import('./pages/auth/auth.module').then(mod => mod.AuthModule),
    data: {
      title: 'Auth',
      description: 'Auth - Member Area',
    }
  },

  {
    path: '**',
    component: PageNotFoundComponent,
    data: {
      title: 'Page Not Found',
      description: 'Page Not Found',
    },
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false,
        relativeLinkResolution: 'corrected',
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
