import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';
import { CardComponent } from './components/card/card.component';
import { HighlightComponent } from './components/highlight/highlight.component';
import { HomeComponent } from './pages/home/components/home.component';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './components/pageNotFound/page-not-found.component';
import { FirebaseModule } from '../firebase.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EventsService } from './services/events.service';
import { AuthService } from './services/auth.service';
import { SeoService } from './services/seo.service';

const components = [
  AppComponent,
  HomeComponent,
  PageNotFoundComponent,
  FooterComponent,
  HeaderComponent,
  MenuComponent,
  CardComponent,
  HighlightComponent,
];

const services = [
  EventsService,
  AuthService,
  SeoService
];

@NgModule({
  declarations: [
    ...components
  ],
  imports: [
    BrowserAnimationsModule,
    FirebaseModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AppRoutingModule
  ],
  providers: [
    ...services
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
