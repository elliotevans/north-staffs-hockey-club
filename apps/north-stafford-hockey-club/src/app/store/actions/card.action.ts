import { Action } from '@ngrx/store';

export const LOAD_CARDS = '[INFO] Load Cards';
export const LOAD_CARDS_FAIL = '[INFO] Load Cards';
export const LOAD_CARDS_SUCCESS = '[INFO] Load Cards';
export const LOAD_SPONSOR_CARDS = '[SPONSORS] Load Sponsor Cards';

export class LoadCards implements Action {
  readonly type = LOAD_CARDS;
}

export class LoadCardsFail implements Action {
  readonly type = LOAD_CARDS_FAIL;

  constructor(payload: any) {

  }

}

export class LoadCardsSuccess implements Action {
  readonly type = LOAD_CARDS_SUCCESS;

  constructor(payload: any) {

  }
}

export type LoadCardsAction = LoadCards | LoadCardsFail | LoadCardsSuccess;
