import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
  selector: 'nshc-site-kit',
  templateUrl: './kit.component.html',
  styleUrls: ['kit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KitComponent implements OnInit {
  private readonly _cDR: ChangeDetectorRef;

  constructor(cDR: ChangeDetectorRef) {
    this._cDR = cDR;
  }

  ngOnInit(): void {
    this._cDR.detach();
  }
}
