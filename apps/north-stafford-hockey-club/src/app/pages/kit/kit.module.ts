import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { KitComponent } from './kit.component';

@NgModule({
  declarations: [
    KitComponent
  ],
  providers: [],
  imports: [
    RouterModule.forChild([
      { path: '', component: KitComponent, pathMatch: 'full'}
    ])
  ]
})
export class KitModule {}
