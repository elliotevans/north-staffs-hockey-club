import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from '../../app.module';
import { KitComponent } from './kit.component';
import { KitModule } from './kit.module';
import { appRoutes } from '../../app-routing.module';

let fixture: ComponentFixture<KitComponent>;
let component: KitComponent;

beforeEach(async () => {
  TestBed.configureTestingModule({
    imports: [
      AppModule,
      KitModule,
      RouterModule,
      RouterTestingModule.withRoutes(appRoutes)
    ],
    declarations: [],
    providers: []
  });

  fixture = TestBed.createComponent(KitComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', async () => {
  expect(component).toBeTruthy();
});

afterEach(async () => {
  fixture.destroy();
});
