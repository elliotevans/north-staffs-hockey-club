import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from '../../app.module';
import { LoginModule } from './login.module';
import { LoginComponent } from './login.component';
import { appRoutes } from '../../app-routing.module';

let fixture: ComponentFixture<LoginComponent>;
let component: LoginComponent;

beforeEach(async () => {
  TestBed.configureTestingModule({
    imports: [
      AppModule,
      LoginModule,
      RouterModule,
      RouterTestingModule.withRoutes(appRoutes),
    ],
    declarations: [],
    providers: []
  });

  fixture = TestBed.createComponent(LoginComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', async () => {
  expect(component).toBeTruthy();
});

afterEach(async () => {
  fixture.destroy();
});
