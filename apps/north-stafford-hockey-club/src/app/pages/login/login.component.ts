import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { ForgottenPasswordDialogComponent } from '../../components/dialog/forgotten-password-dialog/forgotten-password-dialog.component';

@Component({
  selector: 'nshc-site-login',
  templateUrl: './login.component.html',
  styleUrls: ['login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit, OnDestroy {
  loggingIn = false;
  errorMessage: string;
  loginForm: FormGroup;
  submitted = false;

  private readonly _router: Router;
  private readonly _formBuilder: FormBuilder;
  private readonly _dialog: MatDialog;
  private readonly _loginSubscriptions: Subscription = new Subscription();
  private readonly _authService: AuthService;
  private readonly _activatedRoute: ActivatedRoute;
  private readonly _cDR: ChangeDetectorRef;

  constructor(router: Router, formBuilder: FormBuilder, dialog: MatDialog, authService: AuthService, activatedRoute: ActivatedRoute, cDR: ChangeDetectorRef) {
    this._router = router;
    this._formBuilder = formBuilder;
    this._dialog = dialog;
    this._authService = authService;
    this._activatedRoute = activatedRoute;
    this._cDR = cDR;
  }

  get loginFormControls() {
    return this.loginForm.controls;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this.loginForm = this._formBuilder.group({
      username: [
        '',
        [
          Validators.required
        ]
      ],
      password: [
        '',
        [
          Validators.required,
        ]
      ],
    });
    this._cDR.detectChanges();

    this._loginSubscriptions.add(
      this.loginForm.valueChanges.subscribe((x) => this._cDR.detectChanges())
    );
  }

  onSubmit(): void {
    this.loggingIn = true;
    this.submitted = true;

    const isEmail = this.loginForm.value.username.includes("@");
    if (isEmail) {
      this._loginSubscriptions.add(
        this._authService.signInWithEmailAndPassword(this.loginForm.value.username,  this.loginForm.value.password)
          .subscribe(
            () => {
              this.loggingIn = false;
              this._router.navigate(['/auth']);
            },
            (error) => {
              this.loggingIn = false;
              this.submitted = false;
              this.errorMessage = error.message;
              this._cDR.detectChanges();
            }
          )
      );
    } else {
      this._loginSubscriptions.add(
        this._authService.signInWithIdAndPassword(this.loginForm.value.username, this.loginForm.value.password)
          .subscribe(
            () => {
              this.loggingIn = false;
              this._router.navigate(['/auth']);
            },
            (error) => {
              this.loggingIn = false;
              this.submitted = false;
              this.errorMessage = error.message;
              this._cDR.detectChanges();
            }
          )
      );
    }

  }

  forgotPassword(): void {
    this._dialog.open(ForgottenPasswordDialogComponent);
  }

  ngOnDestroy(): void {
    this._loginSubscriptions.unsubscribe();
  }

}
