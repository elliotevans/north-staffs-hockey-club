import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from '../../components/dialog/dialog.module';

@NgModule({
  declarations: [
    LoginComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginComponent,
        pathMatch: 'full'
      }
    ]),
    FormsModule,
    ReactiveFormsModule,
    DialogModule
  ]
})
export class LoginModule {}
