import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TeamsComponent } from './components/teams.component';
import { CommonModule } from '@angular/common';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../../../environments/environment';
import { reducer } from '../../reducers/app.reducer';

export const metaReducers: MetaReducer<any>[] = [];

@NgModule({
  declarations: [
    TeamsComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: TeamsComponent,
        pathMatch: 'full'
      }
    ]),
    StoreModule.forRoot(
      { teams: reducer }, { metaReducers }
    ),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ]
})
export class TeamsModule {}
