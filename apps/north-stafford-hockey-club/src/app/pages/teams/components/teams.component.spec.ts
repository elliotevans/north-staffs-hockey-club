import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TeamsComponent } from './teams.component';
import { TeamsModule } from '../teams.module';
import { StoreModule } from '@ngrx/store';
import { reducer } from '../../../reducers/app.reducer';
import { AppModule } from '../../../app.module';
import { appRoutes } from '../../../app-routing.module';

let fixture: ComponentFixture<TeamsComponent>;
let component: TeamsComponent;

beforeAll(async () => {
  TestBed.configureTestingModule({
    imports: [
      AppModule,
      TeamsModule,
      RouterModule,
      StoreModule.forRoot({ teams: reducer }),
      RouterTestingModule.withRoutes(appRoutes)
    ],
    declarations: [],
    providers: []
  });

  fixture = TestBed.createComponent(TeamsComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', async () => {
  expect(component).toBeTruthy();
});
