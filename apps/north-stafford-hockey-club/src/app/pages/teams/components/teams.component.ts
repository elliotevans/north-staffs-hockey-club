import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { Team, TeamSection } from '../../../model/app.model';
import { ITeamStore, TeamStore } from '../../../reducers/app.reducer';
import * as Actions from '../../../actions/app.actions';

@Component({
  selector: 'nshc-site-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['teams.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamsComponent implements OnInit, OnDestroy {
  category$: Observable<TeamSection>;
  team$: Observable<Team>;
  allTeams$: Observable<Array<TeamSection>>;

  activeCategory: string;
  activeTeam: string;

  private _teamList: Array<TeamSection>;

  private readonly _cDR: ChangeDetectorRef;
  private readonly _store: Store<ITeamStore>;
  private readonly _subscriptions: Subscription = new Subscription();

  constructor(cDR: ChangeDetectorRef, store: Store<ITeamStore>) {
    this._cDR = cDR;
    this._store = store;
  }

  ngOnInit(): void {
    this._cDR.detach();

    this._subscriptions.add(this._store.pipe(select((teamsStore: any) => teamsStore.teams)).subscribe((x: ITeamStore) => this._teamList = x.teams ));

    this.allTeams$ = this._store.pipe(select((teamsStore) => teamsStore.teams[TeamStore.TEAMS]));
    this.category$ = this._store.pipe(select((teamsStore) => teamsStore.teams[TeamStore.ACTIVE_CATEGORY]));
    this.team$ = this._store.pipe(select((teamsStore) => teamsStore.teams[TeamStore.ACTIVE_TEAM]));

    this._cDR.detectChanges();
  }

  toggleCategory(category: string): void {
    const selectedSection = this._teamList.find((section) => section.name === category);
    this._store.dispatch(new Actions.SelectSection(selectedSection));
    this._store.dispatch(new Actions.SelectTeam(null));

    this.activeCategory = selectedSection.name;

    this._cDR.detectChanges();
  }

  toggleTeam(team: Team): void {
    this._store.dispatch(new Actions.SelectTeam(team));
    this.activeTeam = team.name;
    this._cDR.detectChanges();
  }

  ngOnDestroy(): void {
    this._subscriptions.unsubscribe();
  }

}
