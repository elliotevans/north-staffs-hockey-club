import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProfileComponent } from './components/profile.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../../../material.module';
import { PasswordPipe } from '../../pipes/password.pipe';
import { MembershipService } from '../../../membership/services/membership.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    ProfileComponent,
    PasswordPipe
  ],
  providers: [
    MembershipService
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProfileComponent,
        pathMatch: 'full'
      }
    ]),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ProfileModule {}
