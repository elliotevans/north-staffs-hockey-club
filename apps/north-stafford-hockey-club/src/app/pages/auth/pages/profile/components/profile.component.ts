import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { map, mergeMap, tap } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../../../../services/auth.service';
import { MembershipService } from '../../../../membership/services/membership.service';

export class Member {
  coach: string;
  comments: string;
  dateOfBirth: string;
  disability: string;
  doctor: string;
  email: string;
  emergencyContactName: string;
  emergencyContactNumber: string;
  emergencyContactRelationship: string;
  ethnicity: string;
  firstAddress: string;
  gender: string;
  givenName: string;
  id: number;
  medicalHistory: number;
  occupation: number;
  password: number;
  phoneNumber: number;
  postcode: number;
  secondAddress: number;
  surname: number;
  townCity: number;
  twitter: number;
  umpire: number;
}

@Component({
  selector: 'nshc-site-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent implements OnInit, OnDestroy {
  memberInformation: Member;
  profileForm: FormGroup;
  noDataMessage: string;

  private readonly _cDR: ChangeDetectorRef;
  private readonly _matSnackBar: MatSnackBar;
  private readonly _userSubscriptions: Subscription = new Subscription();
  private readonly _authService: AuthService;
  private readonly _membershipService: MembershipService;

  private _userId: string;
  private _userEmail: string;
  private _userKey: string;

  constructor(cDR: ChangeDetectorRef, matSnackBar: MatSnackBar, authService: AuthService, membershipService: MembershipService) {
    this._cDR = cDR;
    this._matSnackBar = matSnackBar;
    this._authService = authService;
    this._membershipService = membershipService;

    this.profileForm = new FormGroup({});

    this._userSubscriptions.add(
      this._authService
        .getUser()
        .pipe(
          tap( (user) => {
            if (user) {
              this._userId = user.uid;
              this._userEmail = user.email;
            }
          }),
          mergeMap( () => this._membershipService.getMembership())
        )
        .subscribe((res) => {
          const user = res.docs.filter( (doc) => doc.data()['email'] === this._userEmail);

          if(!user || user.length === 0) {
            this.noDataMessage = "There is no information associated with this user";
            this._cDR.detectChanges();
            return;
          }

          this.memberInformation = (user[0].data() as Member);
          this._userKey = user[0].id;

          Object
            .keys(this.memberInformation)
            .map((key) => {
              if(key === 'dateOfBirth') {
                const splitDateString = this.memberInformation[key].split("/");
                this.memberInformation[key] = `${splitDateString[2]}/${splitDateString[1]}/${splitDateString[0]}`;
                return this.profileForm.addControl(key, new FormControl(new Date(this.memberInformation[key]).toISOString().substring(0, 10)));
              }
              return this.profileForm.addControl(key, new FormControl(this.memberInformation[key]));
            });

          this._cDR.detectChanges();
        })
    );

  }

  get formControls() {
    return this.profileForm.controls;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this._cDR.detectChanges();
  }

  updateProfileInformation(): void {
    this._cDR.detectChanges();
    // Fixing issues with the JS Date format
    this.profileForm.value.dateOfBirth = new Date(this.profileForm.value.dateOfBirth).toLocaleDateString('en-GB');
    this._cDR.detectChanges();

    this._membershipService
      .updateDoc(this._userKey, this.profileForm.value)
      .subscribe( (res) => {
        this._matSnackBar.open('Details have been changed', null, { duration: 3000});
        this._cDR.detectChanges();
      });
  }

  resetPassword() {
    return this._userSubscriptions.add(
     this._authService
      .sendPasswordResetEmail(this._userEmail)
      .subscribe(
        () => this._matSnackBar.open('Password Reset email sent', null, { duration: 3000 }),
        (error) => console.error(error)
      )
    );
  }

  ngOnDestroy(): void {
    this._userSubscriptions.unsubscribe();
  }

}
