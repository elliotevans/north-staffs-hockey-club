import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ProfileComponent } from './profile.component';
import { ProfileModule } from '../profile.module';
import { appRoutes } from '../../../../../app-routing.module';
import { AppModule } from '../../../../../app.module';

let fixture: ComponentFixture<ProfileComponent>;
let component: ProfileComponent;

beforeEach(async () => {
  TestBed.configureTestingModule({
    imports: [
      AppModule,
      ProfileModule,
      RouterModule,
      RouterTestingModule.withRoutes(appRoutes)
    ],
    declarations: [],
    providers: []
  });

  fixture = TestBed.createComponent(ProfileComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', async () => {
  expect(component).toBeTruthy();
});

afterEach(async () => {
  fixture.destroy();
});
