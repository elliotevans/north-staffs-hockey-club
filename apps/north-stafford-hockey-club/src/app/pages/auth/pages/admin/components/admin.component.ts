import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map, mergeMap, tap } from 'rxjs/operators';
import { firestore } from 'firebase';
import { DateOfBirthFormatterHelper } from '../helpers/dateOfBirthFormatter.helper';
import { CreateIdHelper } from '../helpers/createId.helper';
import { PhoneNumberHelper } from '../helpers/phoneNumberHelper';
import { CloudTypes } from '../../../../../types/cloud.types';
import { AuthService } from '../../../../../services/auth.service';
import { MembershipService } from '../../../../membership/services/membership.service';
import { FirestoreCollectionPaths } from '../../../../../types/firestore-collection.types';
import { Member } from '../../profile/components/profile.component';
import { MatDialog } from '@angular/material/dialog';
import { EditMemberDialogComponent } from '../../../../../components/dialog/edit-member-dialog/edit-member-dialog.component';

interface IUserInfo {
  umpire: string;
  townCity: string;
  medicalHistory: string;
  gender: string;
  ethnicity: string;
  disability: string;
  photoConsent: string;
  doctor: string;
  coach: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  dateOfBirth: string;
  postCode: string;
  id: string;
  email: string;
  password: string;
  comments: string;
  firstAddress: string;
  secondAddress: string;
  emergencyContactName: string;
  emergencyContactNumber: string;
  emergencyContactRelationship: string;
  occupation: string;
  twitter: string;
  county: string;
}

@Component({
  selector: 'nshc-site-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminComponent implements OnInit, OnDestroy {
  loadedDatabase = false;
  membershipState: IUserInfo[];
  number: number;

  private members: IUserInfo[];

  private readonly _membership: AngularFirestoreCollection<any>;
  private readonly _cDR: ChangeDetectorRef;
  private readonly _baseUrl: CloudTypes = CloudTypes.FUNCTION;
  private readonly _authService: AuthService;
  private readonly _membershipService: MembershipService;
  private readonly _dialog: MatDialog;

  constructor(aFS: AngularFirestore, cDR: ChangeDetectorRef, authService: AuthService, membershipService: MembershipService, dialog: MatDialog) {
    this._cDR = cDR;
    this._authService = authService;
    this._membershipService = membershipService;
    this._dialog = dialog;
    this._membership = aFS.collection(FirestoreCollectionPaths.MEMBERSHIP);
  }

  ngOnInit(): void {
    this._cDR.detectChanges();
    this._membership
      .get()
      .pipe(
        tap(() => this._cDR.detach()),
        map((membership: firestore.QuerySnapshot) => {
          this.members = membership.docs.map((m) => ({
            firstName: m.data().givenName,
            lastName: m.data().surname,
            dateOfBirth: m.data().dateOfBirth,
            email: m.data().email,
            phoneNumber: m.data().phoneNumber,
            postCode: m.data().postCode,
            id: m.data().id,
            umpire: m.data().umpire,
            townCity: m.data().townCity,
            medicalHistory: m.data().medicalHistory,
            gender: m.data().gender,
            ethnicity: m.data().ethnicity,
            disability: m.data().disability,
            photoConsent: m.data().photoConsent,
            doctor: m.data().doctor,
            coach: m.data().coach,
            password: m.data().password,
            comments: m.data().comments,
            firstAddress: m.data().firstAddress,
            secondAddress: m.data().secondAddress,
            emergencyContactName: m.data().emergencyContactName,
            emergencyContactNumber: m.data().emergencyContactNumber,
            emergencyContactRelationship: m.data().emergencyContactRelationship,
            occupation: m.data().occupation,
            twitter: m.data().twitter,
            county: m.data().county
          }));

          return this.members;
        }),
      )
      .subscribe((res: IUserInfo[]) => {
        this.loadedDatabase = true;
        this.membershipState = res;
        this._cDR.detectChanges();
      });
  }

  exportMembers(): void {
    this._authService
      .getUser()
      .pipe(
        mergeMap((user) =>
          fetch(`${this._baseUrl}/createMembersReport`, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
              'Content-Type': 'application/json; charset=utf-8',
              'Authorization': `Bearer ${user.uid}`,
            },
            redirect: 'follow',
            referrer: user.providerId,
            body: JSON.stringify(this.members),
          }))
      )
      .subscribe(response => response.json());
  }

  formatDateOfBirthAndAddMissingIds(): void {
    this._membership
      .get()
      .pipe(
        tap(() => this._cDR.detach()),
      )
      .subscribe((membership: any) => {
        let index = 0;
        let timeout = 200;

        membership.docs.forEach(m => {
          setTimeout( () => {
            this._membershipService.updateDoc(m.id, {
              id: CreateIdHelper.generateId(m.data().id),
              surname: m.data().surname,
              givenName: m.data().givenName,
              firstAddress: m.data().firstAddress,
              secondAddress: m.data().secondAddress,
              townCity: m.data().townCity,
              county: m.data().county || "Unknown County",
              postcode: m.data().postcode,
              email: m.data().email,
              twitter: m.data().twitter || "No Twitter",
              phoneNumber: PhoneNumberHelper.formatNumber(m.data().phoneNumber),
              dateOfBirth: DateOfBirthFormatterHelper.formatToDashDate(m.data().dateOfBirth),
              gender: m.data().gender,
              ethnicity: m.data().ethnicity,
              disability: m.data().disability,
              occupation: m.data().occupation || "Unknown Occupation",
              doctor: m.data().doctor,
              emergencyContactName: m.data().emergencyContactName || "Unknown Emergency Contact Name",
              emergencyContactNumber: m.data().emergencyContactNumber || "Unknown Emergency Contact Number",
              emergencyContactRelationship: m.data().emergencyContactRelationship || "Unknown Emergency Contact Relationship",
              medicalHistory: m.data().medicalHistory,
              umpire: m.data().umpire,
              coach: m.data().coach,
              comments: m.data().comments || "No comment",
              whereHereUs: m.data().whereHereUs || "Unknown Where Here Us",
              photoConsent: m.data().photoConsent || "Unknown Photo Consent",
              password: m.data().password || "Unknown Password",
              postCode: m.data().postCode || "Unknown Postcode"
            });

            this._cDR.detectChanges();
            index ++;
            this.number = index;
            if(index % 80 === 0) {
              timeout = 2000;
            } else {
              timeout = 200;
            }

          }, timeout);

        });

      });
  }

  ngOnDestroy(): void {}

  editMember(member: Member): void {
    this._dialog.open(EditMemberDialogComponent, {
      data: {
        member
      }
    });
  }

}
