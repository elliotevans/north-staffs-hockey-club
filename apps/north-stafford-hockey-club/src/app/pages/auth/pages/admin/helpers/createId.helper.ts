export class CreateIdHelper {
  static generateId(currentId: number): number {
    if (currentId) return currentId;
    return Math.floor(1000 + Math.random() * 9000);
  }
}
