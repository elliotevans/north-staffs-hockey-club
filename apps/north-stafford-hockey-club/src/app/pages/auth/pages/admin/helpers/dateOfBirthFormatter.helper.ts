export class DateOfBirthFormatterHelper {
  static formatToDashDate(currentDate: string): string {
    if(!currentDate) return "Unknown Date of Birth";
    if(currentDate.match(/[a-zA-Z]/)) currentDate = new Date("10 March 2007").toLocaleDateString();
    if(currentDate.includes("-")) return currentDate;
    let currentSpacing = "/";
    if(currentDate.includes(".")) currentSpacing = ".";

    const splitCurrentDate = currentDate.split(currentSpacing);

    const horribleCode = () => {
      if(splitCurrentDate[2].length === 2) {
        // tslint:disable-next-line:radix
        return parseInt(splitCurrentDate[2]) > 50 ? "19" + splitCurrentDate[2]: "20" + splitCurrentDate[2];
      } else {
        return splitCurrentDate[2];
      }
    };

    return `${horribleCode()}-${splitCurrentDate[1]}-${splitCurrentDate[0]}`;
  }
}
