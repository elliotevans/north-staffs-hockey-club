import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './components/admin.component';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../../../material.module';
import { MembershipService } from '../../../membership/services/membership.service';
import { HttpClientModule } from '@angular/common/http';
import { DialogModule } from '../../../../components/dialog/dialog.module';

@NgModule({
  declarations: [
    AdminComponent,
  ],
  providers: [
    MembershipService
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
    RouterModule.forChild([
      {
        path: '',
        component: AdminComponent,
        pathMatch: 'full'
      }
    ]),
    DialogModule
  ]
})
export class AdminModule {}
