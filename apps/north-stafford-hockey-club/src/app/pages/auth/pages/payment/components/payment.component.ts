import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'nshc-site-payment',
  templateUrl: 'payment.component.html',
  styleUrls: ['./payment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentComponent implements OnInit {
  private readonly _cDR: ChangeDetectorRef;

  constructor(router: Router, cDR: ChangeDetectorRef) {
    this._cDR = cDR;
  }

  ngOnInit(): void {
    this._cDR.detach();
  }

}
