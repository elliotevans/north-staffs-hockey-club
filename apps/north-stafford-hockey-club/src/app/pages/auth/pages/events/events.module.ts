import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EventsComponent } from './components/events.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../../../material.module';

@NgModule({
  declarations: [
    EventsComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([
      {
        path: '',
        component: EventsComponent,
        pathMatch: 'full'
      }
    ]),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EventsModule {}
