import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { DocumentData } from '@angular/fire/firestore';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IEvent } from '../../../../../types/events.type';
import { EventsService } from '../../../../../services/events.service';

@Component({
  selector: 'nshc-site-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventsComponent implements OnInit, OnDestroy {
  events: Array<IEvent> = [];
  eventForm: FormGroup;
  editAnEvent = false;
  createAnEvent = true;
  editedEvent: IEvent;

  private readonly _cDR: ChangeDetectorRef;
  private readonly _afAuth: AngularFireAuth;
  private readonly _eventSubscriptions: Subscription = new Subscription();
  private readonly _formBuilder: FormBuilder;
  private readonly _eventsService: EventsService;

  constructor(cDR: ChangeDetectorRef, afAuth: AngularFireAuth, formBuilder: FormBuilder, eventsService: EventsService) {
    this._cDR = cDR;
    this._afAuth = afAuth;
    this._formBuilder = formBuilder;
    this._eventsService = eventsService;

    this.eventForm = this._formBuilder.group({
      title: [
        '',
        [
          Validators.required,
        ]
      ],
      description: [
        '',
        [
          Validators.required,
        ]
      ],
      information: [
        '',
        [
          Validators.required,
        ]
      ],
      href: [
        '',
        [
          Validators.required,
        ]
      ],
      active: [
        false,
        [
          Validators.required,
        ]
      ],
    });

    this._eventSubscriptions.add(
      this._eventsService
        .valueChanges()
        .subscribe((res) => {
          this.events = res;
          this._cDR.detectChanges();
        })
    );
  }

  get formControls() {
    return this.eventForm.controls;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this._cDR.detectChanges();
  }

  publishEvent(): void {
    if (this.editAnEvent) {
      const eventData: IEvent = {
        id: this.editedEvent.id,
        active: this.eventForm.value.active,
        date: new Date(),
        description: this.eventForm.value.description,
        information: this.eventForm.value.information,
        title: this.eventForm.value.title,
        href: this.eventForm.value.href
      };

      this._eventSubscriptions.add(
        this._eventsService
          .get()
          .subscribe((res) => {
            let id = null;

            res.docs.forEach((doc) => {
              if (doc.data().id === eventData.id) id = doc.id;
            });

            this.editEvent(eventData, id);

          })
      );
    } else {
      const eventData: IEvent = {
        id: Math.floor((Math.random() * 10000) + 1),
        active: this.eventForm.value.active,
        date: new Date(),
        description: this.eventForm.value.description,
        information: this.eventForm.value.information,
        title: this.eventForm.value.title,
        href: this.eventForm.value.href
      };

      this.createEvent(eventData);
    }

  }

  private editEvent(data: IEvent, id: number): void {
    this._eventsService
      .updateDoc(id.toString(), data)
      .subscribe(
        () => alert('Edited'),
        (error) => alert(`Failed to edit: ${error.toString()}`)
      );

    this._cDR.detectChanges();
  }

  private createEvent(data: IEvent): void {
    this._eventsService
      .add(data)
      .subscribe(
        () => alert('Submitted'),
        (error) => alert(`Failed to submit: ${error.toString()}`)
      );

    this._cDR.detectChanges();
  }

  deleteEventGetId(id: number): void {
    this._eventSubscriptions.add(
      this._eventsService
        .get()
        .subscribe((res) => {
          res.docs.forEach((doc) => {
            if (doc.data().id === id) this.deleteEvent(doc.id);
          });
        })
    );
  }

  private deleteEvent(id: string): void {
    this._eventsService
      .deleteDoc(id)
      .subscribe(
        () => alert('Deleted'),
        (error) => alert(`Failed to delete: ${error.toString()}`)
      );

    this._cDR.detectChanges();
  }

  trackById(index, contact) {
    return contact.id;
  }

  toggleEventForm(t: IEvent): void {
    this.editAnEvent = true;
    this.editedEvent = t;
    this.createAnEvent = false;

    this._cDR.detectChanges();
  }

  ngOnDestroy(): void {
    this._eventSubscriptions.unsubscribe();
  }

}
