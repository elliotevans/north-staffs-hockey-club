import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { navLinksList } from '../data/nav.data';
import { AuthLink } from '../types/auth.types';
import { Subscription } from 'rxjs';

@Component({
  selector: 'nshc-site-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthComponent implements OnInit, OnDestroy {
  readonly navLinks: Array<AuthLink> = navLinksList;

  private readonly _cDR: ChangeDetectorRef;
  private readonly _router: Router;
  private readonly _routerSubscription: Subscription = new Subscription();

  constructor(cDR: ChangeDetectorRef, router: Router) {
    this._cDR = cDR;
    this._router = router;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this._cDR.detectChanges();

    this._routerSubscription.add(
      this._router.events.subscribe( (res) => this._cDR.detectChanges())
    );
  }

  setTabAsActive(subRoute: AuthLink): boolean {
    return this._router.url.includes(subRoute.path);
  }

  ngOnDestroy(): void {
    this._routerSubscription.unsubscribe();
  }

}
