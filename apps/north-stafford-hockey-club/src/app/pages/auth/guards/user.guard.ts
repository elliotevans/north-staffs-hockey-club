import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../../../services/auth.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserGuard implements CanActivate {
  private readonly _router: Router;
  private readonly _afAuth: AngularFireAuth;
  private readonly _authService: AuthService;

  constructor(router: Router, afAuth: AngularFireAuth, authService: AuthService) {
    this._router = router;
    this._afAuth = afAuth;
    this._authService = authService;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this._authService
      .isAuthenticated()
      .pipe(map((auth) => {
        if(auth) {
          return true;
        }

        this._router.navigateByUrl('/');
        return false;
      }));
  }
}
