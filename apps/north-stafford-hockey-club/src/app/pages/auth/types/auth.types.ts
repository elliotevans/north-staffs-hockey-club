export interface AuthLink {
  path: string;
  label: string;
}
