import { AuthLink } from '../types/auth.types';

export const navLinksList: Array<AuthLink> = [
  {
    path: 'profile',
    label: 'Profile'
  },
  {
    path: 'payment',
    label: 'Payment'
  },
  {
    path: 'events',
    label: 'Events'
  },
  {
    path: 'admin',
    label: 'Admin'
  }
];
