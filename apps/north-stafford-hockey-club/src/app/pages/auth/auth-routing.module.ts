import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './components/auth.component';
import { AdminGuard } from './guards/admin.guard';
import { UserGuard } from './guards/user.guard';

// TODO: Preload all children as its the admin part of the site

export const authRoutes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
      },
      {
        path: 'profile',
        canActivate: [
          UserGuard
        ],
        loadChildren: () => import('./pages/profile/profile.module').then(mod => mod.ProfileModule),
        data: {
          title: 'Your Profile',
          description: 'Your Profile',
        //  preload: true
        },
      },
      {
        path: 'payment',
        canActivate: [
          UserGuard
        ],
        loadChildren: () => import('./pages/payment/payment.module').then(mod => mod.PaymentModule),
        data: {
          title: 'Payment Area',
          description: 'Payment Area',
          //  preload: true
        },
      },
      {
        path: 'events',
        canActivate: [
          UserGuard,
          AdminGuard
        ],
        loadChildren: () => import('./pages/events/events.module').then(mod => mod.EventsModule),
        data: {
          title: 'Events Area',
          description: 'Events Area',
          //  preload: true
        },
      },
      {
        path: 'admin',
        canActivate: [
          UserGuard,
          AdminGuard
        ],
        loadChildren: () => import('./pages/admin/admin.module').then(mod => mod.AdminModule),
        data: {
          title: 'Admin Area',
          description: 'Admin Area',
          //  preload: true
        },
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(
      authRoutes,
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AuthRoutingModule {}
