import { NgModule } from '@angular/core';
import { AuthComponent } from './components/auth.component';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../material.module';
import { AuthRoutingModule } from './auth-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    AuthRoutingModule,
  ],
  declarations: [
    AuthComponent,
  ],
})
export class AuthModule {}
