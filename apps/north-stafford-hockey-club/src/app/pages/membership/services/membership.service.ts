import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { fromPromise } from 'rxjs/internal-compatibility';
import { firestore } from 'firebase';
import { CloudTypes } from '../../../types/cloud.types';
import { AuthService } from '../../../services/auth.service';
import { IMembership } from '../../../types/membership.types';
import { FirestoreCollectionPaths } from '../../../types/firestore-collection.types';

@Injectable()
export class MembershipService {
  private readonly _http: HttpClient;
  private readonly _baseUrl: CloudTypes = CloudTypes.FUNCTION;
  private readonly _authService: AuthService;
  private readonly _membershipCollection: AngularFirestoreCollection<IMembership>;

  constructor(http: HttpClient, authService: AuthService, aFirestore: AngularFirestore) {
    this._http = http;
    this._authService = authService;
    this._membershipCollection = aFirestore.collection(FirestoreCollectionPaths.MEMBERSHIP);
  }

  getMembership(): Observable<firestore.QuerySnapshot> {
    return this._membershipCollection.get();
  }

  addMembership(membershipForm: any): Observable<DocumentReference> {
    return fromPromise(
      this._membershipCollection
        .add(membershipForm)
    );
  }

  updateDoc(id: string, data: IMembership): Observable<void> {
    return fromPromise(
      this._membershipCollection
        .doc(id)
        .update(data)
    );
  }

  sendMembership(membershipForm: any, email: string, password: string): Observable<any> {
    return this._authService.signInWithEmailAndPassword(email, password)
      .pipe(mergeMap( () => postData(`${this._baseUrl}/sendMembership`, membershipForm, this._authService.getUser())));

    function postData(url, data, auth) {
      return fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': `Bearer ${auth.uid}`,
        },
        redirect: 'follow',
        referrer: auth.u,
        body: JSON.stringify(data),
      })
        .then(response => response.json());
    }

  }

}
