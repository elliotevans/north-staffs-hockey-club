import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MembershipModule } from '../membership.module';
import { MembershipComponent } from './membership.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { MatSnackBar } from '@angular/material/snack-bar';
import { By, HAMMER_LOADER } from '@angular/platform-browser';
import { AppModule } from '../../../app.module';
import { appRoutes } from '../../../app-routing.module';

let fixture: ComponentFixture<MembershipComponent>;
let component: MembershipComponent;

beforeEach(() => {
  TestBed.configureTestingModule({
    imports: [
      AppModule,
      MembershipModule,
      RouterModule,
      RouterTestingModule.withRoutes(appRoutes),
      ServiceWorkerModule.register('', {enabled: false})
    ],
    declarations: [],
    providers: [
      {
        provide: MatSnackBar,
      },
      {
        provide: HAMMER_LOADER,
        useValue: () => new Promise(() => {})
      }
    ]
  });

  fixture = TestBed.createComponent(MembershipComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', () => {
  expect(component).toBeTruthy();
});

void describe('When the user is filling out the form', () => {

  test('then the content form has been created', () => {
    expect(component.membershipForm).toBeDefined();
  });

  it('form invalid when empty', () => {
    expect(component.form.valid).toBeFalsy();
  });

  void describe('And the user has filled out there first name', () => {
    const surnameText = 'Evans';

    beforeEach(() => {
      component.membershipForm.patchValue({surname: surnameText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.surname.value).toBe(surnameText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#surname')).nativeElement.value).toBe(surnameText);
    });

    test('Then the value is valid', () => {
      expect(component.form.surname.valid).toBeTruthy();
    });

  });


  void describe('And the user has filled out there first name incorrectly', () => {
    const surnameText = 'Evans11';

    beforeEach(() => {
      component.membershipForm.patchValue({surname: surnameText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.surname.value).toBe(surnameText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#surname')).nativeElement.value).toBe(surnameText);
    });

    test('Then the value is invalid', () => {
      expect(component.form.surname.invalid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there last name', () => {
    const givenNameText = 'Elliot';

    beforeEach(() => {
      component.membershipForm.patchValue({givenName: givenNameText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.givenName.value).toBe(givenNameText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#givenName')).nativeElement.value).toBe(givenNameText);
    });

    test('Then the value is valid', () => {
      expect(component.form.givenName.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there last name incorrectly', () => {
    const givenNameText = 'Elliot11';

    beforeEach(() => {
      component.membershipForm.patchValue({givenName: givenNameText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.givenName.value).toBe(givenNameText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#givenName')).nativeElement.value).toBe(givenNameText);
    });

    test('Then the value is invalid', () => {
      expect(component.form.givenName.invalid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there first address', () => {
    const firstAddressText = '10 Whitemore Road';

    beforeEach(() => {
      component.membershipForm.patchValue({firstAddress: firstAddressText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.firstAddress.value).toBe(firstAddressText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#firstAddress')).nativeElement.value).toBe(firstAddressText);
    });

    test('Then the value is valid', () => {
      expect(component.form.firstAddress.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there second address', () => {
    const secondAddressText = 'Trentham';

    beforeEach(() => {
      component.membershipForm.patchValue({secondAddress: secondAddressText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.secondAddress.value).toBe(secondAddressText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#secondAddress')).nativeElement.value).toBe(secondAddressText);
    });

    test('Then the value is valid', () => {
      expect(component.form.secondAddress.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there town or city', () => {
    const townCityText = 'Stoke-on-Trent';

    beforeEach(() => {
      component.membershipForm.patchValue({townCity: townCityText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.townCity.value).toBe(townCityText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#townCity')).nativeElement.value).toBe(townCityText);
    });

    test('Then the value is valid', () => {
      expect(component.form.townCity.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there county', () => {
    const countyText = 'Staffordshire';

    beforeEach(() => {
      component.membershipForm.patchValue({county: countyText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.county.value).toBe(countyText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#county')).nativeElement.value).toBe(countyText);
    });

    test('Then the value is valid', () => {
      expect(component.form.county.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there postcode', () => {
    const postcodeText = 'ST4 8AL';

    beforeEach(() => {
      component.membershipForm.patchValue({postcode: postcodeText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.postcode.value).toBe(postcodeText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#postcode')).nativeElement.value).toBe(postcodeText);
    });

    test('Then the value is valid', () => {
      expect(component.form.postcode.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there postcode without spaces', () => {
    const postcodeText = 'ST48AL';

    beforeEach(() => {
      component.membershipForm.patchValue({postcode: postcodeText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.postcode.value).toBe(postcodeText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#postcode')).nativeElement.value).toBe(postcodeText);
    });

    test('Then the value is valid', () => {
      expect(component.form.postcode.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there postcode with upper and lower case', () => {
    const postcodeText = 'St4 8aL';

    beforeEach(() => {
      component.membershipForm.patchValue({postcode: postcodeText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.postcode.value).toBe(postcodeText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#postcode')).nativeElement.value).toBe(postcodeText);
    });

    test('Then the value is valid', () => {
      expect(component.form.postcode.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there email', () => {
    const emailText = 'ell15evans.nuls@googlemail.com';

    beforeEach(() => {
      component.membershipForm.patchValue({email: emailText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.email.value).toBe(emailText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#email')).nativeElement.value).toBe(emailText);
    });

    test('Then the value is valid', () => {
      expect(component.form.email.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there twitter', () => {
    const twitterText = '@test123';

    beforeEach(() => {
      component.membershipForm.patchValue({twitter: twitterText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.twitter.value).toBe(twitterText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#twitter')).nativeElement.value).toBe(twitterText);
    });

    test('Then the value is valid', () => {
      expect(component.form.twitter.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there twitter with the @ symbol', () => {
    const twitterText = '@EIIiotEvans';

    beforeEach(() => {
      component.membershipForm.patchValue({twitter: twitterText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.twitter.value).toBe(twitterText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#twitter')).nativeElement.value).toBe(twitterText);
    });

    test('Then the value is valid', () => {
      expect(component.form.twitter.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there twitter incorrectly', () => {
    const twitterText = 'EIIiot!>Evans';

    beforeEach(() => {
      component.membershipForm.patchValue({twitter: twitterText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.twitter.value).toBe(twitterText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#twitter')).nativeElement.value).toBe(twitterText);
    });

    test('Then the value is invalid', () => {
      expect(component.form.twitter.invalid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there phone number', () => {
    const phoneNumberText = '01782642042';

    beforeEach(() => {
      component.membershipForm.patchValue({phoneNumber: phoneNumberText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.phoneNumber.value).toBe(phoneNumberText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#phoneNumber')).nativeElement.value).toBe(phoneNumberText);
    });

    test('Then the value is valid', () => {
      expect(component.form.phoneNumber.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there date of birth', () => {
    const dateOfBirthText = '1995-11-13';

    beforeEach(() => {
      component.membershipForm.patchValue({dateOfBirth: dateOfBirthText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.dateOfBirth.value).toBe(dateOfBirthText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#dateOfBirth')).nativeElement.value).toBe(dateOfBirthText);
    });

    test('Then the value is valid', () => {
      expect(component.form.dateOfBirth.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there ethnicity', () => {
    const ethnicityText = 'white_british';

    beforeEach(() => {
      component.membershipForm.patchValue({ethnicity: ethnicityText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.ethnicity.value).toBe(ethnicityText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#ethnicity')).nativeElement.value).toBe(ethnicityText);
    });

    test('Then the value is valid', () => {
      expect(component.form.ethnicity.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there disability', () => {
    const disabilityText = 'no_disability';

    beforeEach(() => {
      component.membershipForm.patchValue({disability: disabilityText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.disability.value).toBe(disabilityText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#disability')).nativeElement.value).toBe(disabilityText);
    });

    test('Then the value is valid', () => {
      expect(component.form.disability.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there occupation', () => {
    const occupationText = 'programmer';

    beforeEach(() => {
      component.membershipForm.patchValue({occupation: occupationText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.occupation.value).toBe(occupationText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#occupation')).nativeElement.value).toBe(occupationText);
    });

    test('Then the value is valid', () => {
      expect(component.form.occupation.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there doctor', () => {
    const doctorText = 'who knows';

    beforeEach(() => {
      component.membershipForm.patchValue({doctor: doctorText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.doctor.value).toBe(doctorText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#doctor')).nativeElement.value).toBe(doctorText);
    });

    test('Then the value is valid', () => {
      expect(component.form.doctor.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out emergancy contact name', () => {
    const emergencyContactNameText = 'Barry Evans';

    beforeEach(() => {
      component.membershipForm.patchValue({emergencyContactName: emergencyContactNameText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.emergencyContactName.value).toBe(emergencyContactNameText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#emergencyContactName')).nativeElement.value).toBe(emergencyContactNameText);
    });

    test('Then the value is valid', () => {
      expect(component.form.emergencyContactName.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out emergancy contact number', () => {
    const emergencyContactNumberText = '01782642042';

    beforeEach(() => {
      component.membershipForm.patchValue({emergencyContactNumber: emergencyContactNumberText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.emergencyContactNumber.value).toBe(emergencyContactNumberText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#emergencyContactNumber')).nativeElement.value).toBe(emergencyContactNumberText);
    });

    test('Then the value is valid', () => {
      expect(component.form.emergencyContactNumber.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out emergency contact relationship', () => {
    const emergencyContactRelationshipText = 'parent_guardian';

    beforeEach(() => {
      component.membershipForm.patchValue({emergencyContactRelationship: emergencyContactRelationshipText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.emergencyContactRelationship.value).toBe(emergencyContactRelationshipText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#emergencyContactRelationship')).nativeElement.value).toBe(emergencyContactRelationshipText);
    });

    test('Then the value is valid', () => {
      expect(component.form.emergencyContactRelationship.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out medical history', () => {
    const medicalHistoryText = 'its fine';

    beforeEach(() => {
      component.membershipForm.patchValue({medicalHistory: medicalHistoryText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.medicalHistory.value).toBe(medicalHistoryText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#medicalHistory')).nativeElement.value).toBe(medicalHistoryText);
    });

    test('Then the value is valid', () => {
      expect(component.form.medicalHistory.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out umpire', () => {
    const umpireText = 'yes';

    beforeEach(() => {
      component.membershipForm.patchValue({umpire: umpireText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.umpire.value).toBe(umpireText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#umpire')).nativeElement.value).toBe(umpireText);
    });

    test('Then the value is valid', () => {
      expect(component.form.umpire.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out coach', () => {
    const coachText = 'yes';

    beforeEach(() => {
      component.membershipForm.patchValue({coach: coachText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.coach.value).toBe(coachText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#coach')).nativeElement.value).toBe(coachText);
    });

    test('Then the value is valid', () => {
      expect(component.form.coach.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out comments', () => {
    const commentsText = 'yes';

    beforeEach(() => {
      component.membershipForm.patchValue({comments: commentsText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.comments.value).toBe(commentsText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#comments')).nativeElement.value).toBe(commentsText);
    });

    test('Then the value is valid', () => {
      expect(component.form.comments.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out where did you hear about us', () => {
    const wherehereusText = 'From people';

    beforeEach(() => {
      component.membershipForm.patchValue({whereHereUs: wherehereusText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.whereHereUs.value).toBe(wherehereusText);
    });

    test('Then the value displays in the input', () => {
      expect(fixture.debugElement.query(By.css('#whereHereUs')).nativeElement.value).toBe(wherehereusText);
    });

    test('Then the value is valid', () => {
      expect(component.form.whereHereUs.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out photoConsent', () => {
    const photoConsentText = true;

    beforeEach(() => {
      component.membershipForm.patchValue({photoConsent: photoConsentText});
    });

    test('Then the value matches what the user entered', () => {
      expect(component.form.photoConsent.value).toBe(photoConsentText);
    });

    test('Then the value is valid', () => {
      expect(component.form.photoConsent.valid).toBeTruthy();
    });

  });


  void describe('And the form has been fillied out with only the required fields entered', () => {
    beforeEach(() => {
      component.membershipForm.patchValue({
        surname: 'Evans',
        givenName: 'Elliot',
        firstAddress: '10 Whitmore Road',
        secondAddress: 'Trentham',
        townCity: 'Stoke-on-Trent',
        county: 'Staffordshire',
        postcode: 'ST4 8AL',
        email: 'ell15evans.nuls@googlemail.com',
        phoneNumber: '01782642042',
        dateOfBirth: '1995-11-13',
        gender: 'male',
        ethnicity: 'white_british',
        disability: 'no_disability',
        occupation: 'Programmer',
        emergencyContactName: 'Barry Evans',
        emergencyContactNumber: '01782642042',
        emergencyContactRelationship: 'parent_guardian',
        umpire: 'yes',
        coach: 'no'
      });

      component.onSubmit();
      fixture.detectChanges();
    });

    it('then the form is valid after filling out all details', () => {
      expect(component.membershipForm.valid).toBeTruthy();
    });
  });

});
