import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { DateFormatHelper } from '../helpers/date-format.helper';
import { Subscription } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import ActionCodeSettings = firebase.auth.ActionCodeSettings;
import { fromPromise } from 'rxjs/internal-compatibility';
import { MembershipService } from '../services/membership.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'nshc-site-membership-form',
  templateUrl: './membership.component.html',
  styleUrls: ['membership.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MembershipComponent implements OnInit, OnDestroy {
  membershipForm: FormGroup;
  membershipFormResponse: string;
  submitted = false;
  sendingContactForm: boolean;

  private readonly _formBuilder: FormBuilder;
  private readonly _membershipFormService: MembershipService;
  private readonly  _cDR: ChangeDetectorRef;
  private readonly _afAuth: AngularFireAuth;
  private readonly _membershipSubscription: Subscription = new Subscription();
  private readonly _authService: AuthService;
  private readonly _activatedRoute: ActivatedRoute;
  private readonly _router: Router;

  constructor(formBuilder: FormBuilder,
              membershipFormService: MembershipService,
              cDR: ChangeDetectorRef,
              afAuth: AngularFireAuth,
              authService: AuthService,
              activatedRoute: ActivatedRoute,
              router: Router
  ) {
    this._formBuilder = formBuilder;
    this._membershipFormService = membershipFormService;
    this._cDR = cDR;
    this._afAuth = afAuth;
    this._authService = authService;
    this._activatedRoute = activatedRoute;
    this._router = router;
  }

  get form() {
    return this.membershipForm.controls;
  }

  ngOnInit(): void {
    this.membershipForm = this._formBuilder.group({
      surname: [
        '',
        [
          Validators.required,
          Validators.maxLength(32),
          Validators.pattern("[a-zA-Z\-'\s]+")
        ]
      ],
      givenName: [
        '',
        [
          Validators.required,
          Validators.maxLength(32),
          Validators.pattern("[a-zA-Z\-'\s]+")
        ]
      ],
      firstAddress: [
        '',
        [
          Validators.required
        ]
      ],
      secondAddress: [
        '',
        [
          // Validators.required
        ]
      ],
      townCity: [
        '',
        [
          Validators.required,
          // Validators.pattern('^([a-zA-Z\u0080-\u024F]+(?:. |-| |\'))*[a-zA-Z\u0080-\u024F]*$')
        ]
      ],
      county: [
        '',
        [
          Validators.required,
        ]
      ],
      postcode: [
        '',
        [
          Validators.required,
          // Validators.pattern(postcodePattern)
        ]
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.email
        ]
      ],
      twitter: [
        '',
        [
          Validators.pattern('@([A-Za-z0-9_]{1,15})')
        ]
      ],
      phoneNumber: [
        '',
        [
          Validators.required
        ]
      ],
      dateOfBirth: [
        '',
        [
          Validators.required
        ]
      ],
      gender: [
        '',
        [
          Validators.required
        ]
      ],
      ethnicity: [
        '',
        [
          Validators.required
        ]
      ],
      disability: [
        '',
        [
          Validators.required
        ]
      ],
      occupation: [
        '',
        [
          Validators.required
        ]
      ],
      doctor: [
        '',
        [
          Validators.maxLength(100)
        ],
      ],
      emergencyContactName: [
        '',
        [
          Validators.required,
          Validators.maxLength(32),
          // Validators.pattern('[a-zA-Z\\s]*')
        ]
      ],
      emergencyContactNumber: [
        '',
        [
          Validators.required
        ]
      ],
      emergencyContactRelationship: [
        '',
        [
          Validators.required
        ]
      ],
      medicalHistory: [
        '',
        [
          Validators.maxLength(180)
        ]
      ],
      umpire: [
        '',
        [
          Validators.required
        ]
      ],
      coach: [
        '',
        [
          Validators.required
        ]
      ],
      comments: [
        '',
        [
          Validators.maxLength(180)
        ]
      ],
      whereHereUs: [
        '',
        [
          Validators.maxLength(180)
        ]
      ],
      photoConsent: [
        '',
        [
          // Validators.required
        ]
      ],
    });
  }

  onSubmit(): void {
    const formRequest = this.membershipForm.value;

    this.submitted = true;
    this.sendingContactForm = true;

    if (this.membershipForm.invalid) {
      this.sendingContactForm = false;
      this.membershipFormResponse = 'Something has gone wrong, please check what you have typed is correct';
      this._cDR.detectChanges();
    }

    if (this.membershipForm.valid) {
      this.membershipForm.controls.dateOfBirth.patchValue(DateFormatHelper.changeDateFormat(this.membershipForm.value.dateOfBirth));

      const email = this.membershipForm.value.email;
      const password = Math.random().toString(36).substr(2, 12);
      const emailVerificationSettings: ActionCodeSettings = {
        url: `https://northstaffordhc.co.uk/login/?email=${email}`,
        handleCodeInApp: true
      };

    this._membershipSubscription.add(
        this._authService.createUserWithEmailAndPassword(email, password)
          .pipe(
            mergeMap( (res) => fromPromise(res.user.sendEmailVerification(emailVerificationSettings)) ),
            mergeMap( () => this._membershipFormService.addMembership(formRequest) ),
            mergeMap( () => this._membershipFormService.sendMembership(formRequest, email, password)),
            mergeMap( (res) => this._authService.signOut().pipe(map( () => res))),
          )
          .subscribe(
            (response) => {
              this.sendingContactForm = false;
              // this.membershipFormResponse = response.message;
              this.submitted = false;
              this.membershipForm.reset();
              this.membershipForm.markAsPristine();
              this.membershipForm.markAsUntouched();
              this._cDR.detectChanges();
            },
            (error) => {
              this.sendingContactForm = false;
              this.membershipFormResponse = `Something has gone wrong when Creating your user. Error: ${error}`;
              this._cDR.detectChanges();
            }
          )
      );
    }

  }

  ngOnDestroy(): void {
    this._membershipSubscription.unsubscribe();
  }

}
