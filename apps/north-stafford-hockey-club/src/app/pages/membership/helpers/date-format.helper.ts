export class DateFormatHelper {
  static changeDateFormat(unFormattedDate: string): string {
    const date = new Date(unFormattedDate);

    return [
      date.getFullYear(),
      ('0' + (date.getMonth() + 1)).slice(-2),
      ('0' + date.getDate()).slice(-2)
    ].join('-');
  }
}
