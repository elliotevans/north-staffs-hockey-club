import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MembershipComponent } from './components/membership.component';
import { MembershipService } from './services/membership.service';
import { MaterialModule } from '../../../material.module';
import { HttpClientModule } from '@angular/common/http';
import { FirebaseModule } from '../../../firebase.module';

@NgModule({
  declarations: [
    MembershipComponent
  ],
  providers: [
    MembershipService
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild([
      {
        path: '',
        component: MembershipComponent,
        pathMatch: 'full'
      }
    ]),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FirebaseModule
  ]
})
export class MembershipModule {}
