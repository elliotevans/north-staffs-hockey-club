import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AboutComponent } from './components/about.component';

@NgModule({
  declarations: [
    AboutComponent
  ],
  providers: [
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: AboutComponent,
        pathMatch: 'full'
      }
    ])
  ]
})
export class AboutModule {}
