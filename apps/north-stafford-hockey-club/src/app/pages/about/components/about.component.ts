import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
  selector: 'nshc-site-about',
  templateUrl: './about.component.html',
  styleUrls: ['about.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AboutComponent implements OnInit {
  readonly currentYear: string = new Date().getFullYear().toString();

  private readonly _cDR: ChangeDetectorRef;

  constructor(cDR: ChangeDetectorRef) {
    this._cDR = cDR;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this._cDR.detectChanges();
  }
}
