import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EventComponent } from './components/event.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    EventComponent
  ],
  providers: [
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: EventComponent,
        pathMatch: 'full'
      }
    ]),
    CommonModule
  ]
})
export class EventModule {}
