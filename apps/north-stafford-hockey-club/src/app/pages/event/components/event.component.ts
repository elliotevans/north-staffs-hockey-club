import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { IEvent } from '../../../types/events.type';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { EventsService } from '../../../services/events.service';

@Component({
  selector: 'nshc-site-event',
  templateUrl: './event.component.html',
  styleUrls: ['event.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventComponent implements OnInit {
  content: IEvent;

  private readonly _cDR: ChangeDetectorRef;
  private readonly _route: ActivatedRoute;
  private readonly _eventSubscriptions: Subscription = new Subscription();
  private readonly _eventsService: EventsService;

  constructor(cDR: ChangeDetectorRef, route: ActivatedRoute, eventsService: EventsService) {
    this._cDR = cDR;
    this._route = route;
    this._eventsService = eventsService;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this._cDR.detectChanges();


    this._eventSubscriptions.add(
      this._eventsService
        .valueChanges()
        .subscribe((eventData) => {
          const eventName = this._route.snapshot.paramMap.get('eventName');
          const eventContent = eventData.find(event => event.href === eventName);
          this.content = eventContent.active ? eventContent : null;
          this._cDR.detectChanges();
        })
    );
  }
}
