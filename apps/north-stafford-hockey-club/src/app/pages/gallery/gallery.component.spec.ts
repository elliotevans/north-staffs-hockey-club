import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from '../../app.module';
import { GalleryComponent } from './gallery.component';
import { GalleryModule } from './gallery.module';
import { By } from '@angular/platform-browser';
import { appRoutes } from '../../app-routing.module';

let fixture: ComponentFixture<GalleryComponent>;
let component: GalleryComponent;

const mockImageList = [
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38020610_1958334384206387_6692213192162017280_n.jpg?alt=media&token=d7c41d81-7b30-4a86-911f-7bc612785a19',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38205924_1958334007539758_4368207913009807360_n.jpg?alt=media&token=f7ee6d1c-b2af-4904-a4e4-9816ae68d53e',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38217176_1958336804206145_5169899168382058496_n.jpg?alt=media&token=fe4d77cb-5c2c-423c-a6c7-e8a648904ed9',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38223022_1958333514206474_3742174648175951872_n.jpg?alt=media&token=64262246-70b1-493c-a1f1-b0cdd82e91f8',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38230743_1958333207539838_8488620664289230848_n.jpg?alt=media&token=40635d2f-a81e-4bff-b8ac-5a2fdc3dc9d3',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38231985_1958333707539788_3728552021230354432_n.jpg?alt=media&token=8ca8fede-8906-4f9a-9552-955340f00071',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38253895_1958333950873097_8541105602733015040_n.jpg?alt=media&token=eaf20f02-1d2a-43e8-a450-9993eb27d2b6',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38257380_1958334217539737_7659386294748839936_n.jpg?alt=media&token=e062501b-0a53-47f4-b671-41aaab564cdd',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38268116_1958334077539751_5905823114264051712_n.jpg?alt=media&token=c4f8115d-3008-4f93-9d50-0df294f38610',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38270379_1958334294206396_3659432833362952192_n.jpg?alt=media&token=951a6d03-9a29-47c1-8cd0-48b1e21b62f7',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38270460_1958334047539754_8153118666896965632_n.jpg?alt=media&token=c168527a-515a-4028-9906-196459574335',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38273573_1958333174206508_2450310221399064576_n.jpg?alt=media&token=fdf68969-3f98-4c39-86d3-1ead89c04bd8',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38284399_1958333444206481_8760479716030283776_n.jpg?alt=media&token=959019db-8464-469d-9952-c7593d9942c7',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38303989_1958334334206392_4971263351918166016_n.jpg?alt=media&token=29c396f4-e321-4247-bf8f-00987bdc3f93',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38391196_1958334287539730_4547901742582333440_n.jpg?alt=media&token=aa710598-a720-461f-ab1a-4c28dfbad27a',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38391254_1958333190873173_7345278044854026240_n.jpg?alt=media&token=8e37267e-a085-4337-9acc-ceba471046f4',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38391546_1958333884206437_1712505689726779392_n.jpg?alt=media&token=30ba07cc-6a29-4c01-85c1-2c78870d2bb5',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38392086_1958333874206438_4961182874695892992_n.jpg?alt=media&token=5d589ca3-55bb-498e-96fa-96f48e494c15',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38403329_1958333780873114_6092738476078268416_n.jpg?alt=media&token=dc4ab53f-2342-4ef8-8f2a-1d4c318b5bd3',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38405197_1958333090873183_4585301338543357952_n.jpg?alt=media&token=e3afe80a-5267-44af-b4b8-4edc848eca98',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38412069_1958334000873092_4720835929286115328_n.jpg?alt=media&token=839e583b-f118-4db5-8f73-f2287d2ae3bd',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38422397_1958334197539739_5455673342931501056_n.jpg?alt=media&token=04e4fc9d-4c22-422b-8173-d395ffdad7b1',
  'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/38454830_1958333547539804_8857479654034898944_n.jpg?alt=media&token=eb3b00f3-47a3-4885-907b-a735a4a6f7b0',
];

beforeEach(async () => {
  TestBed.configureTestingModule({
    imports: [
      GalleryModule,
      AppModule,
      RouterModule,
      RouterTestingModule.withRoutes(appRoutes)
    ],
    declarations: [],
    providers: []
  });

  fixture = TestBed.createComponent(GalleryComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', async () => expect(component).toBeTruthy());

void void describe('When the component is initialised', async () => {
  beforeEach(async () => {
    component.ngOnInit();
    fixture.detectChanges();
  });

  test('Then there should be a list of images', async () => expect(component.images).toEqual(mockImageList));
});

void describe('When the component is loaded', async () => {
  beforeEach(async () => {
    component.ngOnInit();
    fixture.detectChanges();
  });

  test('Then there should be a 23 images visible on the page', async () =>
    expect(fixture.debugElement
      .queryAll(By.css('.nshc__card--img')).length)
      .toEqual(fixture.debugElement.componentInstance.images.length));
});

afterEach(async () => fixture.destroy());
