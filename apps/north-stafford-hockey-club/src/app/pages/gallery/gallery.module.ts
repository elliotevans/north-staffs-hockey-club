import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GalleryComponent } from './gallery.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    GalleryComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: GalleryComponent,
        pathMatch: 'full'
      }
    ])
  ]
})
export class GalleryModule {}
