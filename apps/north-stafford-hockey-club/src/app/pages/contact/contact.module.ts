import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ContactComponent } from './contact.component';
import { ContactService } from './contact.service';
import { MapComponent } from '../../components/map/map.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    ContactComponent,
    MapComponent
  ],
  providers: [
    ContactService
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild([
      { path: '', component: ContactComponent, pathMatch: 'full'}
    ]),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ContactModule {}
