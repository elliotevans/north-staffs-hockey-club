import { Injectable } from '@angular/core';
import { IContactForm } from '../../types/types';
import { HttpClient } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable()
export class ContactService {
  private readonly _http: HttpClient;
  private readonly _baseUrl = 'https://europe-west1-north-staffs-hockey-club.cloudfunctions.net';
  private readonly _afAuth: AngularFireAuth;

  constructor(http: HttpClient, afAuth: AngularFireAuth) {
    this._http = http;
    this._afAuth = afAuth;
  }

  sendContactForm(contactForm: IContactForm): Promise<any> {
    return this._afAuth.auth.signInAnonymously()
      .then( () =>
        postData(`${this._baseUrl}/sendEmailConfirmation`,
          contactForm, this._afAuth.auth.currentUser)
          .then(data => data)
          .catch(error => error))
      .catch(error => console.log(error));

    function postData(url, data, auth) {
      return fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': `Bearer ${auth.uid}`,
        },
        redirect: 'follow',
        referrer: auth.u,
        body: JSON.stringify(data),
      })
        .then(response => response.json());
    }

  }

}
