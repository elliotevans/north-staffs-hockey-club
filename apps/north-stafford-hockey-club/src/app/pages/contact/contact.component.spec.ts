import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from '../../app.module';
import { ContactComponent } from './contact.component';
import { ContactModule } from './contact.module';
import { By } from '@angular/platform-browser';
import { appRoutes } from '../../app-routing.module';

let fixture: ComponentFixture<ContactComponent>;
let component: ContactComponent;

beforeEach(async () => {
  TestBed.configureTestingModule({
    imports: [
      ContactModule,
      AppModule,
      RouterModule,
      RouterTestingModule.withRoutes(appRoutes),
    ],
    declarations: [],
    providers: []
  });

  fixture = TestBed.createComponent(ContactComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', async () => {
  expect(component).toBeTruthy();
});

void describe('When the component is initalised', async () => {

  beforeEach(async () => {
    component.ngOnInit();
  });

  test('then the contform form has been created', async () => {
    expect(component.contactForm).toBeDefined();
  });

});

void describe('When the user is filling out the form', async () => {

  test('then the content form has been created', async () => {
    expect(component.contactForm).toBeDefined();
  });

  it('form invalid when empty', async () => {
    expect(component.formControls.valid).toBeFalsy();
  });

  void describe('And the user has filled out there first name', async () => {
    const firstName = 'Elliot';

    beforeEach(async () => {
      component.contactForm.patchValue({firstName: firstName});
    });

    test('Then the value matches what the user entered', async () => {
      expect(component.formControls.firstName.value).toBe(firstName);
    });

    test('Then the value displays in hte input', async () => {
      expect(fixture.debugElement.query(By.css('#firstName')).nativeElement.value).toBe(firstName);
    });

    test('Then the value is valid', async () => {
      expect(component.formControls.firstName.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there first name incorrectly', async () => {
    const firstName = '123 is my name';

    beforeEach(async () => {
      component.contactForm.patchValue({firstName: firstName});
    });

    test('Then the value matches what the user entered', async () => {
      expect(component.formControls.firstName.value).toBe(firstName);
    });

    test('Then the value displays in hte input', async () => {
      expect(fixture.debugElement.query(By.css('#firstName')).nativeElement.value).toBe(firstName);
    });

    test('Then the value is invalid', async () => {
      expect(component.formControls.firstName.valid).toBeFalsy();
    });

  });

  void describe('And the user has filled out there last name', async () => {
    const lastName = 'Evans';

    beforeEach(async () => {
      component.contactForm.patchValue({lastName: lastName});
    });

    test('Then the value matches what the user entered', async () => {
      expect(component.formControls.lastName.value).toBe(lastName);
    });

    test('Then the value displays in hte input', async () => {
      expect(fixture.debugElement.query(By.css('#lastName')).nativeElement.value).toBe(lastName);
    });

    test('Then the value is valid', async () => {
      expect(component.formControls.lastName.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there last name incorrectly', async () => {
    const lastName = '123 is my surname';

    beforeEach(async () => {
      component.contactForm.patchValue({lastName: lastName});
    });

    test('Then the value matches what the user entered', async () => {
      expect(component.formControls.lastName.value).toBe(lastName);
    });

    test('Then the value displays in hte input', async () => {
      expect(fixture.debugElement.query(By.css('#lastName')).nativeElement.value).toBe(lastName);
    });

    test('Then the value is invalid', async () => {
      expect(component.formControls.lastName.valid).toBeFalsy();
    });

  });

  void describe('And the user has filled out there email', async () => {
    const emailAddress = 'ell15evans.nuls@googlemail.com';

    beforeEach(async () => {
      component.contactForm.patchValue({email: emailAddress});
    });

    test('Then the value matches what the user entered', async () => {
      expect(component.formControls.email.value).toBe(emailAddress);
    });

    test('Then the value displays in hte input', async () => {
      expect(fixture.debugElement.query(By.css('#email')).nativeElement.value).toBe(emailAddress);
    });

    test('Then the value is valid', async () => {
      expect(component.formControls.email.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there subject', async () => {
    const subjectMessage = 'Subject for the message';

    beforeEach(async () => {
      component.contactForm.patchValue({subject: subjectMessage});
    });

    test('Then the value matches what the user entered', async () => {
      expect(component.formControls.subject.value).toBe(subjectMessage);
    });

    test('Then the value displays in hte input', async () => {
      expect(fixture.debugElement.query(By.css('#subject')).nativeElement.value).toBe(subjectMessage);
    });

    test('Then the value is valid', async () => {
      expect(component.formControls.subject.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out there subject', async () => {
    const messageText = 'Message here';

    beforeEach(async () => {
      component.contactForm.patchValue({message: messageText});
    });

    test('Then the value matches what the user entered', async () => {
      expect(component.formControls.message.value).toBe(messageText);
    });

    test('Then the value displays in hte input', async  () => {
      expect(fixture.debugElement.query(By.css('#message')).nativeElement.value).toBe(messageText);
    });

    test('Then the value is valid', async  () => {
      expect(component.formControls.message.valid).toBeTruthy();
    });

  });

  void describe('And the user has filled out all details', async  () => {

    beforeEach(async () => {
      component.contactForm.patchValue({
        firstName: 'Elliot',
        lastName: 'Evans',
        email: 'ell15evans.nuls@googlemail.com',
        subject: 'Test Subject',
        message: 'Text Message'
      });

      component.onSubmit();
      fixture.detectChanges();
    });

    it('form is valid after filling out all details', async  () => {
      expect(component.contactForm.valid).toBeTruthy();
    });

  });

});

afterEach(async () => {
  fixture.destroy();
});
