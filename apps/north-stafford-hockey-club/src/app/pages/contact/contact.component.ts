import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from './contact.service';
import { IContactForm } from '../../types/types';

@Component({
  selector: 'nshc-site-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  emailMessage: string;
  sendingContactForm: boolean;
  submitted = false;

  private readonly _formBuilder: FormBuilder;
  private readonly _contactFormService: ContactService;

  constructor(formBuilder: FormBuilder, contactFormService: ContactService) {
    this._formBuilder = formBuilder;
    this._contactFormService = contactFormService;
  }

  get formControls() {
    return this.contactForm.controls;
  }

  ngOnInit(): void {
    this.contactForm = this._formBuilder.group({
      firstName: [
        '',
        [
          Validators.required,
          Validators.maxLength(32),
          Validators.pattern('[a-zA-Z\\s]*')
        ]
      ],
      lastName: [
        '',
        [
          Validators.required,
          Validators.maxLength(32),
          Validators.pattern('[a-zA-Z\\s]*')
        ]
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.email
        ]
      ],
      subject: [
        '',
        [
          Validators.required,
          Validators.maxLength(120)
        ]
      ],
      message: [
        '',
        [
          Validators.required,
          Validators.maxLength(360)
        ]
      ]
    });
  }

  onSubmit(): void {
    this.emailMessage = 'Sending...';
    this.sendingContactForm = true;
    this.submitted = true;

    const { firstName, lastName, email, subject, message } = this.contactForm.value;
    const date = Date();
    const formRequest = {
      firstName: firstName,
      lastName: lastName,
      emailAddress: email,
      subject: subject,
      description: message,
      date: date
    } as IContactForm;

    this._contactFormService.sendContactForm(formRequest)
      .then( (response) => {
        this.emailMessage = response.test;
        this.sendingContactForm = false;
        this.contactForm.reset();
      })
      .catch( (error) => {
        this.emailMessage = error;
        this.sendingContactForm = false;
      });
  }

}
