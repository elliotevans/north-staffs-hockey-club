import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ICard } from '../../../types/image.type';

@Component({
  selector: 'nshc-site-home-view',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {
  sponsors: Observable<Array<ICard>> = of([
    {
      id: 1,
      imageSrc: 'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/Jack-Dulson-Logo.png?alt=media&token=e369eb38-1210-4f0a-92b8-0fac52a32dda',
      imageAlt: 'Jack Dulson Memorial Fund logo',
      text: 'Jack Dulson Memorial Fund',
      href: 'https://jackdulsonmemorialfund.org.uk/'
    },
    {
      id: 2,
      imageSrc: 'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/Swan-Physio-Logo.png?alt=media&token=238accee-ad72-436f-9f4d-af25a9187eef',
      imageAlt: 'Swan Physio logo',
      text: 'Swan Physio',
      href: 'https://www.swanphysio.co.uk/'
    },
    {
      id: 3,
      imageSrc: 'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/orange-tree-logo.jpg?alt=media&token=a1b62f56-344a-4d9b-8f82-d13b503ee8a4',
      imageAlt: 'The Orange Tree logo',
      text: 'The Orange Tree',
      href: 'https://www.theorangetreebarandgrill.co.uk/'
    },
    {
      id: 4,
      imageSrc: 'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/ten-green-bottles.png?alt=media&token=d6834301-bf01-4ccf-a781-32e03cd1141a',
      imageAlt: 'Ten Green Bottles logo',
      text: 'Ten Green Bottles',
      href: 'https://www.facebook.com/TGBNewcastle/'
    }
  ]);

  images: Observable<Array<ICard>> = of([
    {
      id: 1,
      imageSrc: 'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/card_example_2.jpg?alt=media&token=1c8afc1f-c988-4f54-a89b-6612ab3b716c',
      imageAlt: 'North Stafford Player 1',
      text: 'Monday Evening at Newcastle Under Lyme School.',
      href: 'http://northstaffordhc.co.uk/about'
    },
    { id: 2,
      imageSrc: 'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/card_example_3.jpg?alt=media&token=67d64b72-5409-4d5c-9fd8-00fec072c2a4',
      imageAlt: 'North Stafford Player 2',
      text: 'Tuesday Evening at Newcastle Under Lyme School.',
      href: 'http://northstaffordhc.co.uk/about'
    },
    {
      id: 3,
      imageSrc: 'https://firebasestorage.googleapis.com/v0/b/north-staffs-hockey-club.appspot.com/o/card_example_4.jpg?alt=media&token=e40c5ac5-435d-4625-8668-ae238fe8cfec',
      imageAlt: 'North Stafford Player',
      text: 'Open to all, come along and invite your friends, family and work colleagues!',
      href: 'http://northstaffordhc.co.uk/about'
    }
  ]);

  private readonly _cDR: ChangeDetectorRef;

  constructor(cDR: ChangeDetectorRef) {
    this._cDR = cDR;
  }

  ngOnInit(): void {
    this._cDR.detach();
    this._cDR.detectChanges();
  }

}
