import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HomeComponent } from './home.component';
import { By } from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppModule } from '../../../app.module';
import { appRoutes } from '../../../app-routing.module';
import { tap } from 'rxjs/operators';

let fixture: ComponentFixture<HomeComponent>;
let component: HomeComponent;

beforeEach(async () => {
  TestBed.configureTestingModule({
    imports: [
      AppModule,
      RouterModule,
      RouterTestingModule.withRoutes(appRoutes),
      ServiceWorkerModule.register('', {enabled: false})
    ],
    declarations: [],
    providers: [
      {
        provide: MatSnackBar,
      }
    ]
  });

  fixture = TestBed.createComponent(HomeComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

test('component should be created', async () => {
  expect(component).toBeTruthy();
});

void describe('When the component is initialised', async () => {
  beforeEach(async () => {
    component.ngOnInit();
  });

  test('Then true', async () => {
    expect(true).toEqual(true);
  });

});

afterEach(async () => {
  fixture.destroy();
});
