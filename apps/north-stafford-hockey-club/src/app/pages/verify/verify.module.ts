import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerifyGuard } from './guards/verify.guard';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VerifyComponent } from './components/verify.component';
import { MaterialModule } from '../../../material.module';

@NgModule({
  declarations: [
    VerifyComponent
  ],
  providers: [
    VerifyGuard
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: VerifyComponent,
        pathMatch: 'full'
      }
    ]),
  ]
})
export class VerifyModule {}
