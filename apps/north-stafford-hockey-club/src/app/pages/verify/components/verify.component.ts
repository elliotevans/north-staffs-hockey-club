import { ActivatedRoute, Router } from '@angular/router';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { of, Subscription } from 'rxjs';
import { catchError, delay, mergeMap, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'nshc-site-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['verify.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VerifyComponent implements OnInit, OnDestroy {
  verifyPasswordForm: FormGroup;
  isResetPassword = false;
  isVerifyEmail = false;
  submitted: boolean;
  error: any;

  private readonly _activatedRoute: ActivatedRoute;
  private readonly _subscription: Subscription = new Subscription();
  private readonly _authService: AuthService;
  private readonly _formBuilder: FormBuilder;
  private readonly _matSnackBar: MatSnackBar;
  private readonly _router: Router;

  private _code: string;

  constructor(activatedRoute: ActivatedRoute, authService: AuthService, formBuilder: FormBuilder, matSnackBar: MatSnackBar, router: Router) {
    this._activatedRoute = activatedRoute;
    this._authService = authService;
    this._formBuilder = formBuilder;
    this._matSnackBar = matSnackBar;
    this._router = router;
  }

  get form() {
    return this.verifyPasswordForm.controls;
  }

  ngOnInit(): void {

    this.verifyPasswordForm = this._formBuilder.group({
      password: [
        '',
        [
          Validators.required,
          Validators.maxLength(32),
        ]
      ],
    });

    // TODO: use this page to update your password as there isn't any way of updating it until you go into your profile

    // tap( (res) => res.user.updateProfile({displayName: this.membershipForm.value.givenName, photoURL: null})),
    // tap( (res) => res.user.updatePassword(password)),
    // tap( (res) => this._afAuth.auth.updateCurrentUser(res.user)),

    // https://northstaffordhc.co.uk/verify?
    // apiKey=AIzaSyC9yPhOTeT9iKyxSYYm82JgUn0mwXmkH7U
    // &mode=verifyEmail
    // &oobCode=3lLdAJ1K_nE3X-zFEZLaBHDhOlMWnk6Ov0XLVEFAypAAAAFvzpz6Tg
    // &continueUrl=https:%2F%2Fnorthstaffordhc.co.uk%2Flogin%2F

    this._subscription.add(
      this._activatedRoute.queryParams
        .pipe(
          tap( (res) => this._code = res.oobCode),
          mergeMap( (res) => {
            if(res.mode === 'resetPassword') {
              this.isResetPassword = true;
              return of(null);
            } else {
              this.isVerifyEmail = true;
              this._authService.verifyMail(res.oobCode);
              this._router.navigate(['/']);
            }
          })
        )
        .subscribe( (res) => console.log('done', res))
    );
  }

  onSubmit(): void {
    this.submitted = true;

    this._subscription.add(
      this._authService
        .getUser()
        .pipe(
          mergeMap( (user) => this._authService
            .changePassword(this._code, this.verifyPasswordForm.value.password)
            .pipe(catchError(err => {throw new Error(err)}))),
          tap(() => this._matSnackBar.open('Password has successfully changed', null, { duration: 3000 })),
          delay(3000)
        )
        .subscribe(
          () => this._router.navigate(['/']),
          (err) => this.error = err
        )
    )
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

}
