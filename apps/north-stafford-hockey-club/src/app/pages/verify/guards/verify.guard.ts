import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
  providedIn: 'root',
})
export class VerifyGuard implements CanActivate {
  private readonly _router: Router;
  private readonly _afAuth: AngularFireAuth;

  constructor(router: Router, afAuth: AngularFireAuth) {
    this._router = router;
    this._afAuth = afAuth;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    if(route.queryParams !== null) {
      return of(true);
    } else {
      return of(false);
    }
  }
}
