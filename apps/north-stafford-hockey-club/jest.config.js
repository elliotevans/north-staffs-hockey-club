module.exports = {
  name: 'north-stafford-hockey-club',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/north-stafford-hockey-club',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
